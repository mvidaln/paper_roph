dir='upload_4arxiv_final/'
rm -rf $dir
mkdir $dir
mkdir $dir\figs
mkdir $dir\tables

for afile in ./figs/roph_ATCA_2019.pdf ./figs/ROPHW_profiles.pdf ./figs/atca_rgb_85_80_v2.pdf ./figs/atca_rg_85_80.pdf ./figs/SED.pdf ./figs/GSD.pdf ./figs/12-3_80-85countours_b.pdf ./figs/fig_summary_skymem_wpriors.pdf
do
    rsync -va $afile  $dir\figs/
done
for afile in ./tables/intensities_sed_wnotes.tex ./tables/sed_params_joint.tex
do
    rsync -va $afile  $dir\tables/
done

rsync -va report_ATCA_ROPHW_4arxiv.tex  $dir
rsync -va report_ATCA_ROPHW.bbl  $dir

tar cvfz ball4xxx_final.tgz ./$dir
