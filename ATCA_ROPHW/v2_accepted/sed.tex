
\subsection{Spectral energy distribution} 
\label{sec:sed}


The multi-frequency radio maps of $\rho$\,Oph\,W allow for estimates
of its SED between 5 and 39\,GHz. The morphological trends in
frequency should be reflected in variations of the SED between the
emission originating around the 17\,GHz peak and the emission coming
from the vicinity of the 39\,GHz peak.  Such multi-frequency analysis
requires smoothing the data to a common beam, which is that of the
coarsest observations, at 17\,GHz. Once smoothed, we measured the mean
intensity in each map inside the two masks shown in
Fig. \ref{fig:rgb_mask}. Interferometer data are known to be affected
by flux-loss, i.e. missing flux from large angular scales not sampled
by the $uv-$coverage of the interferometer.  By using a prior image
not affected by flux-loss, i.e. as defined in
Appendix\,\ref{sec:appendix}, the {\tt skymem} algorithm allows to
recover such flux loss under the assumption of linear correlation with
the prior. Our simulations using the prior image recovered the missing
flux exactly, but since the cm-wavelength signal does not exactly
follow the prior, biases in the flux-loss correction scheme may affect
the SEDs reported here. We expect such biases to be small and include
them in the absolute calibration error of 10\%, given the tight
correlation with the near-IR tracers and especially with the
IRAC\,8\,$\mu$m image used to build the prior image.


Table\,\ref{tab:intensities} lists the mean intensities $\langle I_\nu
\rangle$ measured within the 17 and 39\,GHz masks, $\mathcal{M}_{17}$
and $\mathcal{M}_{39}$, for the six frequencies that we observed. We
weighted the photometric extraction using the noise image
$\sigma_R(\vec{x})$ given in Eq.\,\ref{eq:noisemosaic}, i.e.
\begin{equation}
  \langle I^k_\nu \rangle = \frac{\sum_{\vec{x}_j \in \mathcal{M}_k} w_R(\vec{x}_j) I_\nu(\vec{x}_j)}{\sum_{\vec{x}_j \in \mathcal{M}_k} w_R(\vec{x}_j) },
\end{equation}
for each mask $\mathcal{M}_k$, and with  $w_R = 1/\sigma^2_R$. The associated error is
\begin{equation}
\sigma(I^k_\nu) = \sqrt{ \frac{ N_{\rm beam} }{\sum_{\vec{x}_j \in \mathcal{M}_k} w_R(\vec{x}_j) }},
\end{equation}
where $N_{\rm beam}$ is the number of pixels in a beam.  The same SEDs
are also plotted in Fig.\,\ref{fig:sed}, where we have  included a
conservative 10\% systematic uncertainty.



When including the 10\% absolute flux calibration uncertainty, the
difference between the SEDs extracted in the two photometric apertures
is not significant. Only the 39\,GHz average intensities appear to
differ at 2$\sigma$. Table\,\ref{tab:intensities} nonetheless lists
the ratio between the measured intensities in each region
I$^{39}_{\nu}/$I$^{17}_{\nu}$, as this ratio systematically increases
with frequency, which may reflect the morphological trends.  In
Fig.\,\ref{fig:sed}, the spectra of the two regions show a steep drop
after reaching the peak, at a frequency of $\sim 30$\,GHz.  We can
notice that the difference in measured intensity between the two
regions is largest at 39\,GHz. The emission from the 39\,GHz mask
shows a spectrum brighter at higher frequencies than the emission
coming from the 17\,GHz mask. This is interesting as the 39\,GHz mask
is shifted towards the direction of the illuminating star HD\,147889.


%\input{tables/intensities_sed_ori.tex}
%\input{tables/intensities_sed_.tex}
%\input{tables/intensities_sed_NO_CAL_error.tex}
\input{tables/intensities_sed_wnotes.tex}
\begin{figure}
  \begin{center}
     \includegraphics[angle=90,width=0.49\textwidth]{figs/SED.pdf}
  \end{center}
  \caption{SED of the $\rho$\,Oph\,W filament measured between 5.5 and
    39\,GHz within the two apertures shown in
    Fig. \ref{fig:rgb_mask}. The red and blue lines correspond to the
    best fit spinning dust models.}
  \label{fig:sed}
\end{figure}


\subsubsection{SED modeling} 
\label{sec:sed_modelling}

Previous works have shown that the cm-wave emission from this region
is dominated by EME and does not have major contributions from
synchrotron or free-free emission, and that its SED on degree angular
scales is adequately fit by spinning-dust models \citep[see
  e.g.][]{cas08,Planck2011A&A...536A..20P,Arce-Tord2020MNRAS.495.3482A}.
Here we ask the question of what are the consequences of the SED that
we measure with ATCA, on arc-minute scales, for the physical
conditions and grain populations within the cloud, and under the
spinning-dust hypothesis. Since the ATCA mosaic of $\rho$\,Oph\,W is
clear of any detectable free-free emission, as shown by the 5\,GHz
map, we used only a spinning dust component, as calculated using the
{\tt SPDUST} code \citep{Ali-Haimoud2009}.

The spinning dust emission depends on a large ($\sim$\,10) number of
parameters that determine environmental properties: the gas density
(n$_{\rm H}$), the gas temperature (T), the intensity of the radiation
field (parameterised in terms of the starlight intensity relative to
the average starlight background, $\chi$), the ionized hydrogen
fractional abundance $x_{\rm{H}} \equiv n_{\rm{H^+}}/n_{\rm{H}}$, the
ionized carbon fractional abundance $x_{\rm{C}} \equiv
n_{\rm{C^+}}/n_{\rm{H}}$. In addition, the spinning dust emissivities
also depend on the grain micro-physics, such as the grain size
distribution and the average dipole moment per atom for the dust
grains. We will assume that the emission detected by ATCA is
originated by spinning PAHs. The motivation for this assumption is the
excellent correlation between the radio emission and the 8\,$\mu$m map
in this region
\citep{cas08,Arce-Tord2020MNRAS.495.3482A}.

In order to fit the ATCA data using the SPDUST code, we fixed some of
the parameters that are well constrained in the literature for this
region. \citet{Habart2003} modeled the mid-IR line emission using a
PDR code and derived physical parameters for the $\rho$\,Oph\,W
filament. Using their results, we fixed the gas temperature and the
intensity of the radiation field. For the ionized Hydrogen and Carbon
abundances, we took the idealized values for PDRs that are listed in
\citet{dl98b}. We then fitted the SEDs using only 3 free parameters:
gas density (n$_H$), average dipole ($\beta$) and an additional
parameter of the grain size distribution ($a_{\rm cutoff}$) that
represents the minimum PAH size that is present in the region. This
last parameter is necessary to avoid shifting the spinning dust peak
to frequencies higher than $\sim30$\,GHz, as predicted by spinning
dust models for an ISM dust distribution in dense conditions such as
in this PDR. We note that some of the parameters in SPDUST are
expected to be highly correlated, for example the gas density,
temperature and radiation field. We avoid these degeneracies by fixing
most of the parameters to the physical conditions already inferred for
this region.

In SPDUST, the grain size distribution is parameterised as in
\citet{Weingartner2001}, where the contribution from PAHs is
characterized by two log-normal distributions. A typical curve using
standard parameters for the Milky Way is shown in black in
Fig. \ref{fig:gsd}. We introduced the $a_{\rm cutoff}$ parameter in
order  to adjust the region of the grain size distribution
that is most relevant to the spinning dust emission: the population of
the smallest grains. This additional parameter $a_{\rm cutoff}$
corresponds to a characteristic size below which we apply an
exponential cutoff modulating the size distribution, so effectively
defining a minimum size for the PAHs.


The data in the SEDs were fitted using the IDL routine {\tt mpfitfun}
\citep{MPFIT_2009}, that uses the Levenberg-Marquardt least-squares
fit to a function. We performed the fit using the SPDUST model for the
two regions shown in Fig. \ref{fig:rgb_mask}. The result from this
initial fit gave very similar values between the two regions for
$n_{\rm H}$ ($3.0\pm1.5$ vs $3.1\pm2.7$) and $\beta$ ($38.7\pm5.9$ vs
$35.1\pm8.4$). We thus decided to fix $n_{\rm H}$ and $\beta$ and only
fit for $a_{\rm cutoff}$.  Fig. \ref{fig:sed}  compares the best
fit SPDUST2 model curves with the SED data points.  


\begin{figure}
  \begin{center}
     \includegraphics[angle=90,width=0.49\textwidth]{figs/GSD.pdf}
  \end{center}
  \caption{Grain size distribution for sizes around 1\,nm for 3
    cases. In black is the prescription from \citet{Weingartner2001}
    for typical Milky Way parameters. In red and blue are the
    distributions arising from the spinning dust fit in the two
    studied regions. In these cases, a cutoff is needed in order to
    fit the SEDs in Fig. \ref{fig:sed}.}
  \label{fig:gsd}
\end{figure}



The result of our fits are summarised in
Table\,\ref{tab:fit_params}. The key parameter to account for the
observed maximum at 30\,GHz is $a_{\rm cutoff}$, without which the
peak would shift towards $\sim$90\,GHz if fixing the physical
conditions to those determined independently in this PDR by
\citet{Habart2003}.  The difference in the free parameter, $a_{\rm
  cutoff}$, between the two regions is only 1.8\,$\sigma$ but it seems
to go in the direction expected in a PDR.  The minimum grain size
$a_{\rm cutoff}$ is slightly larger in the fit of the 17\,GHz mask
data. This means that in this region, there is a slightly lower
abundance of the smallest grains, compared to the other region.  This
behaviour is in agreement with intuition, as the 39\,GHz mask is more
exposed to the radiation from HD\,147889, which can result in a larger
number of the smallest PAHs due to the fragmentation of larger
ones. This provides a possible interpretation for the strong
morphological differences with frequency, which is reflected in the
local SEDs. \citet{Vidal2020MNRAS.495.1122V} recently concluded that
variations in the grain size distribution are also needed to explain
spinning dust morphology in the translucent cloud LDN\,1780.


The difference in $a_{\rm cutoff}$ between the two regions used to
link the multi-frequency morphological variations with spectral trends
may seem small. However, it is consistent with equipartition of
rotational energy \citep[e.g. Eq.\,13 and Eq.\,2
  in][respectively]{dl98b,Dickinson2018NewAR..80....1D}, which
suggests that a reduction in grain size from 6.3\,nm to 6.0\,nm would
shift equipartition rotation frequencies $\tilde{\nu}$ from 30\,GHz to
31.5\,GHz. The spectrum will be shifted accordingly, since for a
Boltzmann distribution of rotation frequencies, the emergent emissivity
$j_\nu/n_H$ is modulated by a high frequency Boltzmann cutoff
$\exp\left[-\frac{3}{2} \nu^2 / \tilde{\nu}^2 \right]$ \citep[see
  Eq.\,63 in][]{dl98b}.


Most of the SPDUST2 parameters were kept fixed in the optimization
summarised in Table\,\ref{tab:fit_params}. Yet some of these
parameters are expected to vary with depth into the PDR, and most
particularly the radiation field. Variations in $\chi$ may also play a
role in the spectral variations between the two SED extractions. We
tested for the impact of such variations by optimizing a model for the
SED in the 17\,GHz mask in which we decreased the UV field from our
default value of $\chi$=400, to $\chi=100$. The result was a slightly
better fit, with reduced $\chi^2_r=1.5$, and $a_{\rm{cutoff}} = 6.2
\pm 0.5$\r{A}. Therefore, even with a $\times$4 variation in the
intensity of the UV field, an increasing $a_{\rm{cutoff}}$ deeper into
the PDR seems to be a robust prediction of the SPDUST2 models.

%\input{tables/sed_params_ori.tex}
\input{tables/sed_params_joint.tex}




Further support for an increasing PAH size deeper into the PDR can be
found in a comparison with the {\em WISE} bands centred on 12\,$\mu$m
and 3.4$\mu$m, which each correspond to PAH bands and whose ratio is a
proxy for PAH size \citep{allamandola+85, ricca+12, croiset+16}.  The
relatively coarse angular resolution of the {\em WISE} images
(6.1\arcsec at 3.4$\mu$m and 15\arcsec at 12$\mu$m), compared to
IRAC\,8$\mu$m (2.5\arcsec), prevents their filtering for the ATCA+{\tt
  skymem} response. But we can nonetheless degrade the {\em WISE}
images to the coarsest ATCA beam (at 17\,GHz) for a multi-frequency
comparison. The smoothed images are not exactly comparable to the ATCA
mosaics, since we have not filtered for the ATCA response. But we hope
that any resulting bias in the following analysis is small, since our
synthesis imaging strategy corrects for missing ATCA antenna spacings
using a prior image in {\tt skymem}, and the main source of PSF
sidelobes is due to flux loss from missing antenna spacings at the
center of the $uv$-plane. We used the {\em WISE} images postprocessed
as in \citep[][]{Arce-Tord2020MNRAS.495.3482A} to produce
Fig.\,\ref{fig:12_3}, which illustrates that the gradient in peak
frequency across the filament is coincident with an increasing {\em
  WISE} 12\,$\mu$m/3.4$\mu$m ratio.  We quantify this trend using a
standard Pearson correlation test $r$ \citep[e.g. same as $r_{\rm
    sky}$ in Eq.\,11 of][]{Arce-Tord2020MNRAS.495.3482A}, so similar
to $r_w$ in Eq.\,\ref{eq:rw} but without the weights, and instead
adjusting the field of extraction to avoid the noise at the edge of
the ATCA mosaics. The resulting Pearson $r$ are listed in
Table\,\ref{table:r_WISE}. We recover the same trend as in
Table\,\ref{table:xcorr}, both $r$ and $r_w$ point at 20\,GHz as the
best match to IRAC\,8$\mu$m. However, the ATCA map that best traces
the shorter {\em WISE} wavelength is 39\,GHz. The excellent
correlation between ATCA 33/39\,GHz with  3.4\,$\mu$m and also
between ATCA 20/17\,GHz with the 8\,$\mu$m template confirm the strong
correlation between AME and PAH emission in this region.


The AME-PAH connection was put in doubt by \citet{Hensley2016} based
on a full-sky analysis on angular scales of 1\,deg. Indeed, when taken
as a whole, the $\rho$\,Oph cloud is a good example of the breakdown
of the correlation between PAHs tracers and AME, since S\,1, the
brightest nebula in the complex in IRAC\,8$\mu$m and also in {\em
  Spitzer}-IRS 11.3$\mu$m PAH band \citep[][their Table\,2]{cas08},
has no detectable EME signal. However, it appears that EME and PAHs do
correlate very tightly in higher angular resolution observations, and  in
regions where EME is present. Another example of excellent
correspondence between AME and PAH emission is shown clearly in
LDN1246 \citep{Scaife2010}.


\begin{figure}
  \begin{center}
     \includegraphics[angle=0,width=0.49\textwidth]{figs/12-3_80-85countours_b.pdf}
  \end{center}
  \caption{Comparison between the ATCA continuum and a proxy for PAH
    size. We show an overlay of a 17\,GHz contour (in red,
    at 85\%-peak) and a 39\,GHz contour (in green, at 80\%-peak), on the ratio
    of {\em WISE} 12$\mu$m to 3.4$\mu$m. The increasing value of the ratio
    deeper into the PDR is consistent with the larger PAHs.}
  \label{fig:12_3}
\end{figure}


  
%It is worth noting that the measured intensities for both regions at
%20\,GHz lie higher than the best fit curves. While this difference is
%less than a couple of $1\sigma$ error bars, these uncertainties are
%meant to reflect the 10\% absolute calibration error, but both 17GHz
%and 20GHz were acquired simultaneously in the same CABB
%setup. Therefore the relative calibration error between 17\,GHz and
%20\,GHz is much smaller as it depends on the quality of the bandpass
%correction, and not on the flux calibration. It is difficult to
%reconcile both measurements and the spinning dust curve using the
%thermal errors only () . Therefore, unless there was a problem with
%the bandpass correction (which does not seem to affect the rest of the
%data), we believe that there is room for this offset may be a real
%feature of the observations.


%Instead, we are now focussing on the 17GHz/20GHz spectral index, which
%should not be affected by the 10% calibration uncertainty since they
%were acquired simultaneously (in the same CABB correlator setup). This
%spectral index seeems to be significantly steeper than that of the
%model curves. We have replaced this material with the following Para:


%It seems that the observed spectral index between 17 and 20\,GHz,
%$\alpha_{17{\rm GHz}}^{21\rm{GHz}}=4.5\pm0.01$, is higher than that
%expected from the theoretical spinning dust curves
%$\bar{\alpha}_{17{\rm GHz}}^{21{\rm GHz}}=2.2$.  Since both 17GHz and
%20GHz were acquired simultaneously in the same CABB setup, their
%spectral index is not affected by their common flux calibration
%uncertainty, and we quote only the thermal error. However, this
%spectral index measurement is also limited by the accuracy of the
%bandpass calibration, which is difficult to ascertain. If the
%difference is real, it might have an origin in a complex spectral
%behaviour currently not included in SPDUST, such as the contribution
%from PAH rotational lines, as described in \citet{Ali-Haimoud2014}.


The predictions for the grain-size distribution reported here should
also be compared to the IR spectra available for $\rho$\,Oph\,W. Under
the spinning dust hypothesis for EME, a complete model should
reproduce simultaneously the radio SED as well as the IR spectra,
which are both due to the same underlying dust population. Here we
limit the scope of this report on the new ATCA observations to only
the radio part, highlighting the need for a future modeling effort.





\begin{table}
\centering
\caption{ATCA - {\em WISE} correlation statistics}
\label{table:r_WISE}
\begin{tabular}{lcc}
\hline 
     $\nu^a$ & {\em WISE}\,3.4$\mu$m   &  IRAC\,8$\mu$m    \\ \hline
     17481 &   0.01$^b$  & 0.82   \\   
     20160 &   0.24  & 0.90   \\   
     33157 &   0.91  & 0.68   \\   
     39157 &   0.93  & 0.44   \\ \hline
\end{tabular}
\begin{flushleft}
  $^a$ Centre frequency in MHz. 
  $^b$ Pearson $r$ coefficients. All values bear a 1$\sigma$  uncertainty of 0.04.
  \end{flushleft}
\end{table}
