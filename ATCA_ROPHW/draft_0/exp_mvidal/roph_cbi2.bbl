\begin{thebibliography}{}

\bibitem[\protect\citeauthoryear{{Ami Consortium}, {Scaife}, {Hurley-Walker},
  {Green}, {Davies}, {Grainge}, {Hobson}, {Lasenby}, {L{\'o}pez-Caniego},
  {Pooley}, {Saunders}, {Scott}, {Titterington}, {Waldram} \& {Zwart}}{{Ami
  Consortium} et~al.}{2009}]{2009MNRAS.394L..46A}
{Ami Consortium} {Scaife} A.~M.~M.,  {Hurley-Walker} N.,  {Green} D.~A.,
  {Davies} M.~L.,  {Grainge} K.~J.~B.,  {Hobson} M.~P.,  {Lasenby} A.~N.,
  {L{\'o}pez-Caniego} M.,  {Pooley} G.~G.,  {Saunders} R.~D.~E.,  {Scott}
  P.~F.,  {Titterington} D.~J.,  {Waldram} E.~M.,    {Zwart} J.~T.~L.,  2009,
  \mnras, 394, L46

\bibitem[\protect\citeauthoryear{{Casassus}, {Cabrera}, {F{\"o}rster},
  {Pearson}, {Readhead} \& {Dickinson}}{{Casassus}
  et~al.}{2006}]{2006ApJ...639..951C}
{Casassus} S.,  {Cabrera} G.~F.,  {F{\"o}rster} F.,  {Pearson} T.~J.,
  {Readhead} A.~C.~S.,    {Dickinson} C.,  2006, \apj, 639, 951

\bibitem[\protect\citeauthoryear{{Casassus}, {Dickinson}, {Cleary}, {Paladini},
  {Etxaluze}, {Lim}, {White}, {Burton}, {Indermuehle}, {Stahl} \&
  {Roche}}{{Casassus} et~al.}{2008a}]{2008MNRAS.391.1075C}
{Casassus} S.,  {Dickinson} C.,  {Cleary} K.,  {Paladini} R.,  {Etxaluze} M.,
  {Lim} T.,  {White} G.~J.,  {Burton} M.,  {Indermuehle} B.,  {Stahl} O.,
  {Roche} P.,  2008a, \mnras, 391, 1075

\bibitem[\protect\citeauthoryear{{Casassus}, {Dickinson}, {Cleary}, {Paladini},
  {Etxaluze}, {Lim}, {White}, {Burton}, {Indermuehle}, {Stahl} \&
  {Roche}}{{Casassus} et~al.}{2008b}]{cas08}
{Casassus} S.,  {Dickinson} C.,  {Cleary} K.,  {Paladini} R.,  {Etxaluze} M.,
  {Lim} T.,  {White} G.~J.,  {Burton} M.,  {Indermuehle} B.,  {Stahl} O.,
  {Roche} P.,  2008b, \mnras, 391, 1075

\bibitem[\protect\citeauthoryear{{Davies}, {Dickinson}, {Banday}, {Jaffe},
  {G{\'o}rski} \& {Davis}}{{Davies} et~al.}{2006}]{dav06}
{Davies} R.~D.,  {Dickinson} C.,  {Banday} A.~J.,  {Jaffe} T.~R.,  {G{\'o}rski}
  K.~M.,    {Davis} R.~J.,  2006, \mnras, 370, 1125

\bibitem[\protect\citeauthoryear{{Dickinson}, {Davies}, {Allison}, {Bond},
  {Casassus}, {Cleary}, {Davis}, {Jones}, {Mason}, {Myers}, {Pearson},
  {Readhead}, {Sievers}, {Taylor}, {Todorovi{\'c}}, {White} \&
  {Wilkinson}}{{Dickinson} et~al.}{2009}]{dic09}
{Dickinson} C.,  {Davies} R.~D.,  {Allison} J.~R.,  {Bond} J.~R.,  {Casassus}
  S.,  {Cleary} K.,  {Davis} R.~J.,  {Jones} M.~E.,  {Mason} B.~S.,  {Myers}
  S.~T.,  {Pearson} T.~J.,  {Readhead} A.~C.~S.,  {Sievers} J.~L.,  {Taylor}
  A.~C.,  {Todorovi{\'c}} M.,  {White} G.~J.,    {Wilkinson} P.~N.,  2009,
  \apj, 690, 1585

\bibitem[\protect\citeauthoryear{{Draine} \& {Lazarian}}{{Draine} \&
  {Lazarian}}{1998a}]{dra98}
{Draine} B.~T.,  {Lazarian} A.,  1998a, \apjl, 494, L19+

\bibitem[\protect\citeauthoryear{{Draine} \& {Lazarian}}{{Draine} \&
  {Lazarian}}{1998b}]{dl98b}
{Draine} B.~T.,  {Lazarian} A.,  1998b, \apj, 508, 157

\bibitem[\protect\citeauthoryear{{Finkbeiner}}{{Finkbeiner}}{2004}]{fin04}
{Finkbeiner} D.~P.,  2004, \apj, 614, 186

\bibitem[\protect\citeauthoryear{{Habart}, {Boulanger}, {Verstraete}, {Pineau
  des For{\^e}ts}, {Falgarone} \& {Abergel}}{{Habart}
  et~al.}{2003}]{2003A&A...397..623H}
{Habart} E.,  {Boulanger} F.,  {Verstraete} L.,  {Pineau des For{\^e}ts} G.,
  {Falgarone} E.,    {Abergel} A.,  2003, \aap, 397, 623

\bibitem[\protect\citeauthoryear{{Kogut}, {Banday}, {Bennett}, {Gorski},
  {Hinshaw} \& {Reach}}{{Kogut} et~al.}{1996}]{kog96}
{Kogut} A.,  {Banday} A.~J.,  {Bennett} C.~L.,  {Gorski} K.~M.,  {Hinshaw} G.,
    {Reach} W.~T.,  1996, \apj, 460, 1

\bibitem[\protect\citeauthoryear{{Leitch}, {Readhead}, {Pearson} \&
  {Myers}}{{Leitch} et~al.}{1997}]{lei97}
{Leitch} E.~M.,  {Readhead} A.~C.~S.,  {Pearson} T.~J.,    {Myers} S.~T.,
  1997, \apjl, 486, L23+

\bibitem[\protect\citeauthoryear{{Liseau}, {White}, {Larsson}, {Sidher},
  {Olofsson}, {Kaas}, {Nordh}, {Caux}, {Lorenzetti}, {Molinari}, {Nisini} \&
  {Sibille}}{{Liseau} et~al.}{1999}]{1999A&A...344..342L}
{Liseau} R.,  {White} G.~J.,  {Larsson} B.,  {Sidher} S.,  {Olofsson} G.,
  {Kaas} A.,  {Nordh} L.,  {Caux} E.,  {Lorenzetti} D.,  {Molinari} S.,
  {Nisini} B.,    {Sibille} F.,  1999, \aap, 344, 342

\bibitem[\protect\citeauthoryear{{Mathis}, {Mezger} \& {Panagia}}{{Mathis}
  et~al.}{1983}]{mat83}
{Mathis} J.~S.,  {Mezger} P.~G.,    {Panagia} N.,  1983, \aap, 128, 212

\bibitem[\protect\citeauthoryear{{Ridge}, {Di Francesco}, {Kirk}, {Li},
  {Goodman}, {Alves}, {Arce}, {Borkin}, {Caselli}, {Foster}, {Heyer},
  {Johnstone}, {Kosslyn}, {Lombardi}, {Pineda}, {Schnee} \& {Tafalla}}{{Ridge}
  et~al.}{2006}]{rid06}
{Ridge} N.~A.,  {Di Francesco} J.,  {Kirk} H.,  {Li} D.,  {Goodman} A.~A.,
  {Alves} J.~F.,  {Arce} H.~G.,  {Borkin} M.~A.,  {Caselli} P.,  {Foster}
  J.~B.,  {Heyer} M.~H.,  {Johnstone} D.,  {Kosslyn} D.~A.,  {Lombardi} M.,
  {Pineda} J.~E.,  {Schnee} S.~L.,    {Tafalla} M.,  2006, \aj, 131, 2921

\bibitem[\protect\citeauthoryear{{Watson}, {Rebolo},
  {Rubi{\~n}o-Mart{\'{\i}}n}, {Hildebrandt}, {Guti{\'e}rrez},
  {Fern{\'a}ndez-Cerezo}, {Hoyland} \& {Battistelli}}{{Watson}
  et~al.}{2005}]{wat05}
{Watson} R.~A.,  {Rebolo} R.,  {Rubi{\~n}o-Mart{\'{\i}}n} J.~A.,  {Hildebrandt}
  S.,  {Guti{\'e}rrez} C.~M.,  {Fern{\'a}ndez-Cerezo} S.,  {Hoyland} R.~J.,
  {Battistelli} E.~S.,  2005, \apjl, 624, L89

\end{thebibliography}
