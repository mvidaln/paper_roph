% mn2esample.tex
%
% v2.1 released 22nd May 2002 (G. Hutton)
%
% The mnsample.tex file has been amended to highlight
% the proper use of LaTeX2e code with the class file
% and using natbib cross-referencing. These changes
% do not reflect the original paper by A. V. Raveendran.
%
% Previous versions of this sample document were
% compatible with the LaTeX 2.09 style file mn.sty
% v1.2 released 5th September 1994 (M. Reed)
% v1.1 released 18th July 1994
% v1.0 released 28th January 1994

\documentclass[useAMS,usenatbib]{mn2e}


\usepackage{epsfig,graphicx,graphics}


\bibliographystyle{mn2e}

\usepackage{aas_macros}
% If your system does not have the AMS fonts version 2.0 installed, then
% remove the useAMS option.
%
% useAMS allows you to obtain upright Greek characters.
% e.g. \umu, \upi etc.  See the section on "Upright Greek characters" in
% this guide for further information.
%
% If you are using AMS 2.0 fonts, bold math letters/symbols are available
% at a larger range of sizes for NFSS release 1 and 2 (using \boldmath or
% preferably \bmath).
%
% The usenatbib command allows the use of Patrick Daly's natbib.sty for
% cross-referencing.
%
% If you wish to typeset the paper in Times font (if you do not have the
% PostScript Type 1 Computer Modern fonts you will need to do this to get
% smoother fonts in a PDF file) then uncomment the next line
% \usepackage{Times}

%%%%% AUTHORS - PLACE YOUR OWN MACROS HERE %%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Observations of the continuum radiation from interstellar carbon

\title[Resolved observations of the centimeter-wavelength continuum
  from the $\rho$~Ophiuci molecular cloud, I: Local emissivity boost
  in the $\rho$~Oph~W photo-dissociation-region ]{Centimeter-wave
  continuum radiation from $\rho$~Oph} \author[Casassus et al.]{ Simon
  Casassus$^{1}$\thanks{E-mail: simon@das.uchile.cl (SC)}, et al.,
%  Glenn J. White$^{2,3}$, 
%  Jamie Stevens,
%  Michael Burton$^6$,  \newauthor  
%  Clive Dickinson$^{2}$, 
%  Kieran Cleary$^{3}$, 
%%  Roberta Paladini$^{2}$,  \newauthor
%  $^{1}$  Departamento de Astronom\'{\i}a, Universidad de Chile, Casilla 36-D,
%  Santiago, Chile\\ 
%  $^{2}$ Infrared Processing and Analysis Center, California Institute of Technology, M/S 220-6,  
%1200 E. California Blvd., Pasadena, CA 91125. \\
%  $^{3}$ Chajnantor Observatory, M/S 105-24,  California Institute of Technology, Pasadena, CA 91125 \\
%  $^{4}$ Department of Physics and Astronomy, The Open University, Milton Keynes MK7 6AA, UK\\
%  $^{5}$ The Rutherford Appleton Laboratory, Didcot, Oxfordshire OX11 0QX, UK. \\
%  $^{6}$ School of Physics, University of New South Wales, Sydney NSW 2052, Australia\\
 }



\begin{document}

\date{}
\pagerange{\pageref{firstpage}--\pageref{lastpage}} \pubyear{2006}

\maketitle

\label{firstpage}

\begin{abstract}
\begin{itemize}
\item The CBI2 mosaic of $\rho$~Oph confirms the CBI1 morphology, and
  highlights the complete absence of detectable 31~GHz signal from
  S~1, the brightest IR nebula in the complex. The spinning dust
  emissivity per nucleon varies by two-orders of magnitudes.
\item The ATCA+CABB mosaic of $\rho$~Oph reveals spectral variations
  in the cm-wave continuum.  At 30~arcsec resolutions, the 17~GHz and
  20~GHz intensities in the $\rho$~Oph~W filament follow tightly
  $I_\mathrm{cm} \propto I(8~\mu$m), despite its breakdown on
  10~arcmin scales.  Despite the breakdown of $I_\mathrm{cm} \propto
  I(8~\mu$m) on 10~arcmin scales, the 8-20~GHz filament seen by ATCA
  at 30~arcsec resolutions follows tightly {\em
    IRAC~}8~$\mu$m. However, the 33-39~GHz filament is parallel to
  {\em IRAC~}8~$\mu$m, but offset by 15--20~arcsec towards the UV
  source.



\end{itemize}
\end{abstract}

%The main source of
%residual charge in $\rho$~Oph~W is the photoionization of carbon by UV
%photons above 11.3~eV.

\begin{keywords}
radiation mechanisms: general, radio continuum: general ISM,  sub-millimetre,
ISM: clouds,
\end{keywords}



\section{Introduction}

Since 1996, experiments designed to measure CMB anisotropy have found
an anomalous diffuse foreground in the range of 10-90~GHz. This
diffuse emission is correlated with thermal emission from dust grains
at 100~$\mu$m on large angular scales, and at high galactic latitudes.
The spectral index (considering $S_\nu \propto \nu^\alpha$) of the
radio-IR correlated signal is $\alpha_\mathrm{radio/IR} \sim 0$ in the
range 15-30~GHz, and so reminiscent of optically thin free-free
\citep{kog96}. But $\alpha_\mathrm{radio/IR} \sim -0.85$ between
20-40~GHz, for high-latitude cirrus \citep{dav06}. The absence of
H$\alpha$ emission concomitant to radio free-free, would require an
electron temperature $T_e \geqslant 10^6$ to quench H\,{\sc i}
recombination lines \citep{lei97}.


Another possibility was presented by \citet{dra98}. They calculated
that spinning interstellar very small dust grains produce rotational
dipole emission in the range from 10 to 100 GHz, at levels that could
account for the excess diffuse foreground observed over the free-free
component.


Observations of specific targets may shed light on the anomalous
foreground, whose existence in the diffuse ISM is inferred
statistically. Cm-wavelength radiation well in excess of the predicted
levels from free-free, synchrotron and Rayleigh-Jeans dust emission,
has been found in a dozen clouds \citep[][]{fin04, wat05,
  2006ApJ...639..951C, 2008MNRAS.391.1075C, 2009MNRAS.394L..46A,
  dic09, vid10}. The observed spectral energy distributions (SEDs) are
a qualitative match to spinning dust models.


In particular $\rho$~Oph~W, the region of the $\rho$~Oph molecular
cloud exposed to UV radiation from HD~147889, is among the nearest
example of a photo-dissociation-region (PDR) in a star forming region,
at a distance of 135~pc.  It is seen edge-on and extends over
10$\times$3~arcmins. $\rho$~Oph is a region of intermediate-mass star
formation. It does not host a conspicuous H\,{\sc ii} region, by
contrast to the Orion Bar, another well studied PDR, where UV fields
are 100 times stronger. $\rho$~Oph~W has been extensively studied in
the far-IR atomic lines observed by ISO
\citep[][]{1999A&A...344..342L, 2003A&A...397..623H}. Observations of
the bulk molecular gas in $\rho$~Oph, through $^{12}$CO(1-0) and
$^{13}$CO(1-0), are publicly available from the COMPLETE database
\citep[][]{rid06}.


Cosmic Background Imager (CBI) observations showed that the
suprisingly bright cm-wave continuum from $\rho$~Oph, for a total {\em
  WMAP}~33~GHz flux density of $\sim$20~Jy, stems from $\rho$~Oph~W
and is fit by spinning dust models \citep[][]{cas08}. However, the
peak at all IR wavelengths, i.e. the circumstellar nebula around S~1,
is undetectable in the CBI data. Upper limits on S~1 flux density and
a cross-correlation with {\em Spitzer}-IRAC~8$\mu$m rule out a linear
radio/IR relationship within the CBI 45~arcmin primary beam (covering
the bulk $\rho$~Oph by mass).




Dust emissivities in the IR are roughly proportional to the energy
density of UV radiation: $I_\mathrm{IR} \propto U N_d$, where
$U$\footnote{ The dimensionless parameter $U$ measures the average
  intensity of radiation in 4$\pi$~sr, integrated from 0.09 to
  8~$\mu$, in units of the corresponding intensity in the solar
  neighbourhood \citep{mat83}} measures the dust-heating radiation
field, and $N_d$ is the column of IR emitters.  A linear relationship,
$I_\mathrm{cm} \propto N_d$ could be proposed, inspired from the
radio-IR correlated signal in Cirrus clouds. The universality of
the spinning dust emissivity per dust grain is marginally consistent
with the CBI data. 


Here we report on CBI2 observations of $\rho$~Oph. We confirm that
$I_\mathrm{cm} \propto N_d$ breaks down on scales smaller than 1~deg,
and that there are strong cm-wavelength emissivity variations in
$\rho$~Oph. In the framework of the spinning dust interpretation,
these emissivity variations may hold the key to identify the dominant
grain spin-up mechanisms. The structure of this article is as
follows: \section{sec:cbiobs} describes the CBI2
observations, \section{sec:discussion} analyses the data,
and \section{sec:conc} concludes.


\section{CBI2 Observations} \label{sec:cbiobs}

Pending: log of observations****. 

The CBI2 MEM mosaic shown on Fig.~\ref{fig:globalrgb} confirms the
intriguing absence of any detectable 31~GHz signal from S~1, the
brightest IR nebula in $\rho$~Oph. Relative to $\rho$~Oph~W, S~1 is
too faint at 31~GHz for a constant spinning dust emissivity per
H-nucleus. As described in \citep[][]{cas08}, the ratio
\[
\Rightarrow   R = G_\circ \times  I_\nu(31\mathrm{GHz})
/I(\mathrm{PAH}~11.3~\mu\mathrm{m})  
\]
should be constant for a constant spinning dust emissivity per
H-nucleus.  The spinning dust emissivities are remarkably independent
of environment \citep[varying at most a factor of 10 over 4 orders of
  magnitude in UV density*** update***][]{dl98b}, while the PAH
intensities are $\propto G_\circ$ (the local UV field).  

Given {\em Spitzer}~IRS spectroscopy of the 11.3~$\mu$m PAH band,
$G_\circ$ from {\em ISO}, and a 3~$\sigma$ upper limit for S~1 from
the CBI2 mosaic, we find that $R$ is at least 42 times greater in
$\rho$~Oph~W than in S~1. 

Therefore environmental factors boost the spinning dust emissivities
in $\rho$~Oph~W, and quench it in S~1. 


\begin{figure}
\caption{\label{fig:globalrgb} Overview of the $\rho$~Oph cloud. Red:
  MIPS~24~$\mu$m, green: IRAC~8~$\mu$m, blue: processed 2MASS~Ks. The
  circles indicate the locations of the 3 earliest stars in the field
  - HD147889 is the westernmost. S~1 lies towards the East, and SR~3
  towards the South.  Countours follow an MEM model of the 31~GHz
  continuum measured by CBI2, and the black and white rectangles are
  the available Spitzer apertures.  $x-$ and $y-$ axis are offset
  J2000 RA and DEC, in degrees of arc.  }
\begin{center}
\includegraphics[width=0.45\textwidth,height=!]{multi_roph_S1_SR3_rgb_CBI2.pdf}
\end{center}
\end{figure}

\section{Local excitation of spinning dust} \label{sec:local}


Local excitation of grain rotation to $\sim$30~GHz frequencies may be
due to radiative torques, increasing with UV, parametrised with
in units of the  intensity from  $G_\circ$ ******

The exciting star of $\rho$~Oph~W, HD147889, is located towards the SW
in Fig.~\ref{fig:globalrgb}, as reflected in the layered structure of the
IR tracers, as that of an IR rainbow. Deeper into the molecular core,
the UV radiation field from HD147889 is attenuated, and the emergent
continuum progressively shifts into the far-IR.


Mid-IR dust emission, due to VSG fluorescence, is proportional to the
local UV field: $I_\mathrm{IR} \propto U N_\mathrm{VSG}$, where
$N_\mathrm{VSG}$ stands for the column of VSGs.  Spinning dust is
proportional to the column of VSG, and theoretical models show it is
fairly independent of the local UV field. If there were no further
environmental factors on the spinning dust emissivity, then the radio
signal should reach much deeper into the $\rho$~Oph~W PDR - it should
at least follow MIPS~24~$\mu$m.

Additionally \citet{cas08} have shown that the brightest IR nebulae in
$\rho$~Oph have no radio counterpart. The absence of radio sources
coincident with the IR-bright circumstellar dust about S~1 and SR~3
can only marginally be reconciled with VSG depletion. 

Another explanation, also suggested from the calculations by
\cite{dra98} ****PENDING: theoretical results from SPDUST model*****,
is that the dominant source of rotational excitation is ``plasma
drag'', due to the interaction of the grain dipoles with passing ions
- namely C$^+$ in the context of PDRs. If so the spinning dust
emissivities would be best understood in terms of an emission measure,
rather than VSG column: $I_\mathrm{cm} \propto N_\mathrm{VSG}
N(\mathrm{C}+)$. 

The absence of S~1 and SR~3 in the radio maps of $\rho$~Oph is then
naturally explained by the fact that these stars are too cold to
create conspicuous C\,{\sc ii} regions \citep[][]{cas08}. 











\appendix 



\section[]{Image reconstruction} \label{sec:appendix}

The traditional image-reconstruction algorithm `CLEAN' is not well
suited for extended sources. Using the Miriad task `clean' we found
that the CLEAN models gave large residuals, with an intensity
amplitude much greater than that expected from thermal noise, and with
a spatial structure reflecting the convolution of the negative
synthetic side-lobes with the morphology of the source. Attempts to
improve dynamic range using Miriad's `maxen' task gave worse
results. We therefore attempted to design a special-purpose image
reconstruction algorithm, based on sky-plane deconvolution, which we
describe in this appendix.


To obtain a model sky image that fits the data we need to solve the
usual deconvolution problem, i.e. obtain the model image $I^m$ 
that minimize a merit function $L$: 
\begin{equation}
L = \sum_i \left[ \left( I_i - I^m A_i  \times B_i  \right)^2 w_i   \right] + S(I^m),
\end{equation}
where the sum extends over the number of fields, $A_i$ is the primary
beam attenuation, $B_i$ is the synthetic beam, $I_i$ the dirty map,
$w_i = 1 / \sigma_i^2$, with $\sigma_i$ the noise map as calculated
with the `sensitivity' option to Miriad's task `invert', and $S(I^m)$
is a regularizing term. After several attempts with a variety of
functional forms for $S$, we found that we obtained best results with
a pure $\chi^2$ optimization, i.e., with $S = 0$. This choice results
in models that fit the noise, which we have to bear in mind for the
subsequent analysis. Trials with a mock dataset simulated on the {\em
  IRAC}~8~$\mu$m image of $\rho$~Oph, with identical $uv$-plane
coverage as the CABB observations, showed that we obtain best results
with pure $\chi^2$ reconstructions (the optimal $I_m$ approximates
best the input {\em IRAC}~8~$\mu$m image, in a least-squares sense,
for a variety of initial conditions). 

It is well known that incomplete sampling in the $uv$-plane, and in
particular the lack of total power measurements, leads to flux
loss. However, if a good template of the image exists, then the
missing fourier components can be approximately recovered in $I^m$. 









%\begin{thebibliography}{99}

%\bibitem[\protect\citeauthoryear{Andr\'e et al. }{1988}]{and88}
%  Andr\'e, P., Montmerle, T., Feigelson, E. D., Stine, P. C., \&
%  Klein, K.-L. 1988, ApJ, 335, 940
%
%
%\bibitem[\protect\citeauthoryear{Abergel et al.}{1996}]{abe96} Abergel, A., et al., 1996, A\&A, 315, L329. 
%
%\bibitem[\protect\citeauthoryear{Baart et al.}{1980}]{baa80} Baart,
%  E.E., de Jager, G., Mountfort, P.I., A\&A, 1980, 92, 156.
%
%%\bibitem[\protect\citeauthoryear{Baker \& Menzel}{1938}]{bak38} Baker,
%%  J.G., Menzel, D.H., 1938, ApJ, 88, 52
%
%\bibitem[\protect\citeauthoryear{Barsony et al.}{1997}]{bar97}
%  Barsony, M., Kenyon, S. J., Lada, E. A., \& Teuben, P. J. 1997, ApJS,
%  112, 109 
%
%\bibitem[\protect\citeauthoryear{Behrend \& Maeder}{2001}]{beh01}
%  Behrend, R., Maeder, A., 2001, A\&A, 373, 190
%
%\bibitem[\protect\citeauthoryear{Bennett}{2003}]{ben03}
%	Bennett, C. L., Halpern, M., Hinshaw, G., Jarosik, N., Kogut,
%        A., Limon, M., Meyer, S. S., Page, L., Spergel, D. N., Tucker,
%        G. S., Wollack, E., Wright, E. L., Barnes, C., Greason, M. R.,
%        Hill, R. S., Komatsu, E., Nolta, M. R., Odegard, N., Peiris,
%        H. V., Verde, L., Weiland, J. L., 2003, ApJSS, 148, 1
%
%\bibitem[\protect\citeauthoryear{Bernard  et al.}{1993}]{ber93}
%  Bernard, J.P., Boulanger, F., Puget, J.L., 1993, A\&A 277, 609.
%
%\bibitem[\protect\citeauthoryear{Bern\'e  et al.}{2007}]{ber07} Bern\'e et al. 2007, A\&A, 469, 575
%
%
%\bibitem[\protect\citeauthoryear{Black \& van Dishoeck}{1987}]{bla87}
%  Black, J.H., van Dishoeck, E.F., 1987, Apj, 322, 412
%
%\bibitem[\protect\citeauthoryear{Bontemps et al.}{2001}]{bon01}
%  Bontemps, S., et al., A\&A, 372, 173.
%
%
%
%\bibitem[\protect\citeauthoryear{Boulanger \& Perault}{1988}]{bou88}
%  Boulanger, F., Perault, M., 1988, ApJ, 330, 964
%
%\bibitem[\protect\citeauthoryear{Boulanger et al.}{1996}]{bou96}
%  Boulanger, F., et al., 1996, A\&A, 315, L325. % 16 coauthors
%
%\bibitem[\protect\citeauthoryear{Brown et al.}{1974}]{bro74} Brown, R.L., Gammon, R.H., Knapp, G.R., Balick, B., 1974, ApJ, 192, 607. 
%
%\bibitem[\protect\citeauthoryear{Brown \& Knapp}{1974}]{bro74a} Brown,  R.L., K%napp, G.R., 1974, ApJ, 189, 253
%
%%\bibitem[\protect\citeauthoryear{Brown \& Zuckerman}{1975}]{bro75}   Brown, R.L., Zuckerman, B., 1975, ApJL, 202, 125
%
%
%\bibitem[\protect\citeauthoryear{Calabretta}{1991}]{cal91} Calabretta, M.R., 1991, AuJPh, 44, 441 
%
%\bibitem[\protect\citeauthoryear{Casassus et al.}{2004}]{cas04}
%  Casassus, S., Readhead, A.C.S., Pearson, T.J., Nyman, L.-\AA,
%  Shepherd, M.C., Bronfman, L., 2004, ApJ, 603, 599
%
%
%
%\bibitem[\protect\citeauthoryear{Casassus et al.}{2006}]{cas06} Casassus, S.,
%  Cabrera, G. F., F\"orster, F., Pearson, T. J., Readhead, A. C. S.,
%  Dickinson, C., 2006, ApJ, 639, 951

%\bibitem[\protect\citeauthoryear{Casassus et al.}{2007}]{cas07}
%  Casassus, S., Nyman, L.-\AA., Dickinson, C., Pearson, T. J., 2007,
%  MNRAS, 382, 1607

%\bibitem[\protect\citeauthoryear{Casassus et al.}{2008}]{cas08}
%  Casassus et al. 2008, MNRAS, 391, 1075


%\bibitem[\protect\citeauthoryear{Cartwright et al.}{2005}]{car05}
%  Cartwright, J. K., Pearson, T. J., Readhead, A. C. S., Shepherd,
%  M. C., Sievers, J. L., Taylor, G. B., 2005, ApJ, 623, 11
%
%\bibitem[\protect\citeauthoryear{Castelli}{2003}]{cas03} Castelli, F.,
%  Kurucz, R. L., 2003, IAU symp. 210, A20.
%
%\bibitem[\protect\citeauthoryear{Chan}{2001}]{cha01} Chan, S. J., et
%  al., 2001, in 'The Calibration Legacy of the ISO
%  Mission'. Eds. L. Metcalfe \& M.F. Kessler, ESA SP-481
%
%\bibitem[\protect\citeauthoryear{Chrysostomou et al.}{1996}]{chr96}
%  Chrysostomou, A., Clark, S. G., Hough, J. H., Gledhill, T. M., McCall, A., Tamura, M., 1996, MNRAS,
%  278, 449
%
%
%
%\bibitem[\protect\citeauthoryear{Condon el al.}{1993}]{co93} Condon,
%  J.J., Griffith, M.R. \& Wright, A.E., 1993, AJ, 106, 1095
%

%\bibitem[Davies et al.  (2006)]{dav06}
%  Davies, R. D., Dickinson, C., Banday, A. J., Jaffe, T. R., G\'orski,
%  K. M., Davis, R. J., 2006, MNRAS, 370, 1125. 
%
% 
%\bibitem[\protect\citeauthoryear{Dickinson et al.}{2006}]{dic06}
%  Dickinson C., Casassus S., Pineda J. L., Pearson T. J., Readhead
%  A. C. S., Davies R. D., 2006, ApJ, 643, L111
%
%\bibitem[\protect\citeauthoryear{Dickinson et al.}{2003}]{dic03}
%  Dickinson, C., Davies, R. D.; Davis, R. J., 2003, MNRAS, 341, 369
%

%\bibitem[Dickinson et al. (2009)]{dic09}
%  Dickinson, C., Davies, R. D., Allison, J. R.k, Bond, J. R.,  Casassus,
%  S., Cleary, K., Davis, R. J., Jones, M. E., Mason, B. S., Myers,
%  S. T., and 7 coauthors, ApJ, 690, 1585.
%

%\bibitem[\protect\citeauthoryear{Draine  et al.}{2003}]{dra03} Draine, B., 2003, ARAA, 41, 241
%
%\bibitem[\protect\citeauthoryear{Draine \& Bertoldi}{1996}]{dra96}
%  Draine, B. T., Bertoldi, F., 1996, ApJ, 468, 269.
%

%\bibitem[\protect\citeauthoryear{Draine \& Lazarian}{1998a}]{dra98} Draine, B.T., Lazarian, A.,  1998a, ApJL, 494, L19
%
%\bibitem[\protect\citeauthoryear{Draine \& Lazarian}{1998b}]{dl98b} Draine, B.T., Lazarian, A.,  1998b, ApJ, 508, 157
%
%\bibitem[\protect\citeauthoryear{Draine \& Lazarian}{1999}]{dl99}
%  Draine, B.T., Lazarian, A.,  1999, ApJ 512, 740
%
%\bibitem[\protect\citeauthoryear{Draine \& Lazarian}{1999b}]{dra99b}
%  Draine, B.T., Lazarian, A.,  1999, 'Microwave Foregrounds', ASP
%  Conference Series, Vol. 181, A. de Oliveira-Costa and M. Tegmark,
%  eds. 
%
%\bibitem[\protect\citeauthoryear{Draine \& Li}{2001}]{dl01} Draine,
%  B.T., Li, A., 2001, ApJ, 551, 807
%
%\bibitem[\protect\citeauthoryear{Draine \& Li}{2007}]{dl07} Draine,
%  B.T., Li, A., 2007, ApJ, 657, 810 
%
%
%
%\bibitem[\protect\citeauthoryear{Dupac et al.}{2003}]{dup03} Dupac,
%  X., et al., 2003, A\&A, 404, L11.
%
%\bibitem[\protect\citeauthoryear{Elias}{1978}]{eli78} Elias, J.H., ApJ, 1978, 224, 453
%
%
%\bibitem[\protect\citeauthoryear{Encrenaz}{1974}]{enc74} Encrenaz,
%  P.J., 1974, ApJL, 189, 135
%
%\bibitem[\protect\citeauthoryear{Encrenaz}{1974}]{fal81} Falgarone,
%  E., Gilmore, W., 1981, A\&A, 95, 32 
%
%
%
%\bibitem[\protect\citeauthoryear{Ferland et al.}{1998}]{fer98}
%Ferland, G. J. Korista, K.T. Verner, D.A. Ferguson, J.W. Kingdon,
%J.B. Verner, \& E.M. 1998, PASP, 110, 761
%
%\bibitem[Finkbeiner et al.(2002)]{fin02} Finkbeiner, D.P., Schlegel, D.J.,
%Frank, C., Heiles, C., 2002, ApJ, 566, 898
%

%\bibitem[\protect\citeauthoryear{Finkbeiner}{2004}]{fin04} Finkbeiner, D.P., 2004, ApJ, 614, 186, 


%
%\bibitem[\protect\citeauthoryear{Gagn\'e et al.}{2004}]{gag04} Gagn\'e, M.,  Skinner, S.L., Daniel, K. J., 2004, ApJ, 613, 393
%
%\bibitem[\protect\citeauthoryear{Garc\'ia Lario}{2001}]{gar01}
%  Garc\'ia Lario, P., 2001, SAI/2001-030/Rp. Version 1.0. 
%
%
%\bibitem[\protect\citeauthoryear{Gaustad et al.}{2001}]{gau01}
%Gaustad, J. E., McCullough, P. R., Rosing, W., Van Buren, D., 2001, PASP, 113, 1326
%
%\bibitem[\protect\citeauthoryear{Grasdalen et al.}{1973}]{gra73}
%  Grasdalen , G., Strom, K.M., Strom, S.E., 1973, ApJL, 184, 53
%
%\bibitem[\protect\citeauthoryear{Hafner \& Meyer}{1995}]{haf95} 
%Hafner, L.M., Meyer, D.M., 1995, ApJ, 453, 450
%
%
%\bibitem[\protect\citeauthoryear{Habart et al.}{2003}]{hab03} Habart, E.,
%  Boulanger, F., Verstraete, L., Pineau des For\^{e}ts, G., Falgarone,
%  E., Abergel, A., 2003, A\&A, 397, 623
%
%\bibitem[\protect\citeauthoryear{Heiles et al.}{1996}]{hei96} Heiles,
%  C., Bon-Chul, K., Levenson, N.A., Reach, W.T., 1996, ApJ, 462, 326
%
%\bibitem[\protect\citeauthoryear{Hinshaw et al.}{2007}]{hin07} Hinshaw, G., et al., 2007, ApJS, 170, 288
%
%\bibitem[\protect\citeauthoryear{Habing}{1968}]{hab68} Habing, H. J., 1968, Bull. Astr. Inst. Netherlands., 19, 421
%
%\bibitem[\protect\citeauthoryear{Houck \& Smith-Moore}{1988}]{hou88}
%  Houk, N., Smith-Moore, M., 1988, Michigan Spectral Survey, Ann
%  Arbor, Dept. of Astronomy, Univ. Michigan (Vol. 4)
%
%\bibitem[\protect\citeauthoryear{Iglesias-Groth}{2005}]{igl05} Iglesias-Groth, S., 2005, ApJ, 632, 25.
%
%\bibitem[Kogut et al. (1996)]{kog96} Kogut, A., Banday, A. J., Bennett, C. L.,
%  Gorski, K. M., Hinshaw, G. \& Reach, W. T.  1996, \apj, 460, 1

%
%\bibitem[\protect\citeauthoryear{Iglesias-Groth}{2006}]{igl06}
%  Iglesias-Groth, S., 2006, MNRAS, 368, 1925.
%
%\bibitem[\protect\citeauthoryear{Lanz \& Hubeny}{2007}]{lan07}
%  Lanz, T.,  Hubeny, I. 2007, ApJS, 169, 83.
%
%\bibitem[\protect\citeauthoryear{Leitch et al.}{1997}]{lei97} Leitch,
%  E.M., Readhead, A.C.S.,  Pearson,   T.J., Myers, S.T., 1997, ApJL 486, L23
%
%
%
%\bibitem[\protect\citeauthoryear{Lada \& Wilking}{1988}]{lad88} 
%Lada, C.J., Wilking, B.A., 1988, ApJ, 287, 610.
%
%
%\bibitem[\protect\citeauthoryear{Liseau et al.}{1995}]{lis95} Liseau,
%  R., Lorenzetti, D., Molinari, S., et al., 1995, A\&A, 300, 493. 
%
%\bibitem[\protect\citeauthoryear{Liseau et al.}{1999}]{lis99} Liseau,
%  R.,   et al., 1999, A\&A, 344, 342. 
%
%
%\bibitem[\protect\citeauthoryear{Lockman et al.}{1996}]{loc96}
%  Lockman, F.J., Pisano, D.J., Howard, G.J., 1996, ApJ, 472, 173
%
%\bibitem[\protect\citeauthoryear{Lynds}{1962}]{lyn62} Lynds, B.T.,
%  1962, ApJS, 7, 1
%
%\bibitem[\protect\citeauthoryear{Martin}{2007}]{mar07}
%Martin, P.G., 2007, EAS Publications Series, 23, 165.
%
%\bibitem[\protect\citeauthoryear{Mathis et al.}{1983}]{mat83}
%  Mathis, J.S., Mezger, P.G., Panagia, N., 1983, A\&A, 128, 212
%
%
%
%\bibitem[\protect\citeauthoryear{Miville-Deschenes \&
%    Lagache}{2005}]{miv05} Miville-Deschenes., M., Lagache, G., 2005,
%  ApJS, 157, 302.
%
%\bibitem[\protect\citeauthoryear{Nahar \& Pradhan}{1997}]{nah97}
%  Nahar, S.N., Pradhan, A.K., 1997, ApJSS, 111, 339.
%
%\bibitem[\protect\citeauthoryear{Padgett et al.}{2008}]{pad08}
%  Padgett, D. L., et al., 2008, ApJ, 672, 1013. 
%
% 
%
%
%\bibitem[\protect\citeauthoryear{Padin et al.}{2002}]{pad02} Padin, S., et al, 2002, PASP, 114, 83
%
%
%\bibitem[\protect\citeauthoryear{Paodan et al.}{2006}]{pao06} Padoan
%  P., Juvela M., Pelkonen V.-M., 2006, ApJ, 636, 101
%


%\bibitem[\protect\citeauthoryear{Pankonin \& Walmsley}{1978}]{pan78}
%  Pankonin, V., Walmsley, C.M., 1978, A\&A, 64, 333.
%
%\bibitem[\protect\citeauthoryear{Readhead et al.}{2004}]{rea04}  Readhead, A. C. S., et al., 2004, Science, 306, 836
%
%\bibitem[\protect\citeauthoryear{Reich et al.}{1990}]{rei90} Reich,
%  W., Fuerst, E., Reich, P., Reif, K., 1990, A\&AS, 85, 633
%
%\bibitem[\protect\citeauthoryear{Ridge et al.}{2006}]{rid06} Ridge,
%  N.A., et al., 2006, AJ, 131, 2921.
%
%\bibitem[\protect\citeauthoryear{Salem \& Brocklehurst}{1979}]{sal79}
%  Salem, M., Brocklehurst, M., 1979, ApJS, 39, 633. 


%\bibitem[Scaife et al. (2009)]{sca09} Ami
%  Consortium; Scaife, A. M. M., et al., 2009, MNRAS, 394, 46

%
%\bibitem[\protect\citeauthoryear{Schaller et al.}{1992}]{sch92}
%  Schaller, G., Schaerer, D., Meynet, G., Maeder, A., 1992, A\&AS, 96, 269
%
%
%\bibitem[Shepherd(1997)]{she97} Shepherd, M.C., 1997, in Astronomical
%  Data Analysis Software and Systems VI, ed. G~Hunt \& H.E.~Payne, ASP
%  conference series, v125, 77-84 ``Difmap: an interactive program for
%  synthesis imaging''.
%
%\bibitem[\protect\citeauthoryear{Schlegel et al.}{1998}]{sch98}
%  Schlegel, D.J., Finkbeiner, D. P., Davis, M., 1998, ApJ, 500, 525
%
%
%\bibitem[\protect\citeauthoryear{Smith et al.}{2004}]{smi04}
%  Smith, J. D. T., Dale, D. A., Armus, L., et al. 2004, ApJS,
%  154, 199
%
%%^\bibitem[\protect\citeauthoryear{Storey \& Hummer}{1995}]{sto95}
%%Storey, P. J., Hummer, D. G.,  1995, MNRAS, 272, 41
%
%
%\bibitem[\protect\citeauthoryear{Stutzki et al.}{1998}]{stu98}
%  Stutzki, J., Bensch, F., Heithausen, A., Ossenkopfm V., Zielinsky,
%  M., 1998, A\&A, 336, 697. 
%
%\bibitem[\protect\citeauthoryear{Tielens}{2005}]{tie05}
%  Tielens, A.G.G.M., 2005, ``The Physics and Chemistry of the
%  Interstellar Medium'', ISBN 0521826349. Cambridge, UK: Cambridge
%  University Press.
%
%

%\bibitem[Vidal et al. (2010)]{vid10} Vidal, M., Casassus, S.,
%  Dickinson, C., Witt, A., Castellanos, P., Paladini, R., Cleary, K.,
%  Davis, R., Davies, R., Taylor, A., 2010, MNRAS, {\em in prep} 

%\bibitem[Watson et al. (2005)]{wat05} Watson,
%R. A., Rebolo, R., Rubi\~no-Mart\'in, J. A., Hildebrandt, S.,
%Guti\'errez, C. M., Fern\'andez-Cerezo, S., Hoyland, R. J.,
%Battistelli, E. S., 2005, ApJ, 624, 89


%\bibitem[\protect\citeauthoryear{Walmsley \& Watson}{1982}]{wal82}  Walmsley, C.M., Watson, W.D., 1982, ApJ, 260, 317. 
%
%\bibitem[\protect\citeauthoryear{Webster}{1992}]{web92}  Webster, A.,
%  1992, MNRAS, 255, 41
%
%\bibitem[\protect\citeauthoryear{Wheelock et al.}{1991}]{whe91}  Wheelock, et al., 1991, IRAS Sky Survey Atlas Explanatory Supplement..
%
%\bibitem[\protect\citeauthoryear{Witt et al.}{1998}]{wit98} Witt,
%  A.N., Gordon, K.D., Furton, D.G., 1998, ApJL, 501, 111 
%
%\bibitem[\protect\citeauthoryear{Wootten et al.}{1979}]{woo79}
%  Wootten, A., Snell, R., Glassgold, A. E., 1979, ApJ, 234, 876
%
%\bibitem[\protect\citeauthoryear{Wootten et al.}{1978}]{woo78}  Wootten, A.,
%  Evans, N. J., II, Snell, R., vanden Bout, P, 1978, ApJL, 225, 143
%
%
%\bibitem[\protect\citeauthoryear{Young et al.}{2006}]{you06} Young, K.E., et
%  al., 2006, ApJ, 644, 326 
%
%\bibitem[\protect\citeauthoryear{Yui et al.}{1993}]{yui93} Yui, Y.Y.,
%  Nakawaga, T., Doi, Y., Okuda, H., Shibai, H., Nishimura, T., Low,
%  F., 1993, ApJ, 419, L37.
%
%





\bibliography{/Users/simon/texinputs/merged.bib}
	

%\bibitem[Vidal et al. (2010)]{vid10} Vidal, M., Casassus, S.,
%  Dickinson, C., Witt, A., Castellanos, P., Paladini, R., Cleary, K.,
%  Davis, R., Davies, R., Taylor, A., 2010, MNRAS, {\em in prep} 

%\end{thebibliography}


\label{lastpage}

\end{document}
