\begin{thebibliography}{}
\makeatletter
\relax
\def\mn@urlcharsother{\let\do\@makeother \do\$\do\&\do\#\do\^\do\_\do\%\do\~}
\def\mn@doi{\begingroup\mn@urlcharsother \@ifnextchar [ {\mn@doi@}
  {\mn@doi@[]}}
\def\mn@doi@[#1]#2{\def\@tempa{#1}\ifx\@tempa\@empty \href
  {http://dx.doi.org/#2} {doi:#2}\else \href {http://dx.doi.org/#2} {#1}\fi
  \endgroup}
\def\mn@eprint#1#2{\mn@eprint@#1:#2::\@nil}
\def\mn@eprint@arXiv#1{\href {http://arxiv.org/abs/#1} {{\tt arXiv:#1}}}
\def\mn@eprint@dblp#1{\href {http://dblp.uni-trier.de/rec/bibtex/#1.xml}
  {dblp:#1}}
\def\mn@eprint@#1:#2:#3:#4\@nil{\def\@tempa {#1}\def\@tempb {#2}\def\@tempc
  {#3}\ifx \@tempc \@empty \let \@tempc \@tempb \let \@tempb \@tempa \fi \ifx
  \@tempb \@empty \def\@tempb {arXiv}\fi \@ifundefined
  {mn@eprint@\@tempb}{\@tempb:\@tempc}{\expandafter \expandafter \csname
  mn@eprint@\@tempb\endcsname \expandafter{\@tempc}}}

\bibitem[\protect\citeauthoryear{{Ami Consortium} et~al.,}{{Ami Consortium}
  et~al.}{2009}]{2009MNRAS.394L..46A}
{Ami Consortium} et~al., 2009, \mn@doi [\mnras]
  {10.1111/j.1745-3933.2008.00607.x}, \href
  {http://adsabs.harvard.edu/abs/2009MNRAS.394L..46A} {394, L46}

\bibitem[\protect\citeauthoryear{{Casassus}, {Cabrera}, {F{\"o}rster},
  {Pearson}, {Readhead}  \& {Dickinson}}{{Casassus}
  et~al.}{2006}]{2006ApJ...639..951C}
{Casassus} S.,  {Cabrera} G.~F.,  {F{\"o}rster} F.,  {Pearson} T.~J.,
  {Readhead} A.~C.~S.,   {Dickinson} C.,  2006, \mn@doi [\apj]
  {10.1086/499517}, \href {http://adsabs.harvard.edu/abs/2006ApJ...639..951C}
  {639, 951}

\bibitem[\protect\citeauthoryear{{Casassus} et~al.,}{{Casassus}
  et~al.}{2008a}]{2008MNRAS.391.1075C}
{Casassus} S.,  et~al., 2008a, \mn@doi [\mnras]
  {10.1111/j.1365-2966.2008.13954.x}, \href
  {http://adsabs.harvard.edu/abs/2008MNRAS.391.1075C} {391, 1075}

\bibitem[\protect\citeauthoryear{{Casassus} et~al.,}{{Casassus}
  et~al.}{2008b}]{cas08}
{Casassus} S.,  et~al., 2008b, \mn@doi [\mnras]
  {10.1111/j.1365-2966.2008.13954.x}, \href
  {http://adsabs.harvard.edu/abs/2008MNRAS.391.1075C} {391, 1075}

\bibitem[\protect\citeauthoryear{{Davies}, {Dickinson}, {Banday}, {Jaffe},
  {G{\'o}rski}  \& {Davis}}{{Davies} et~al.}{2006}]{dav06}
{Davies} R.~D.,  {Dickinson} C.,  {Banday} A.~J.,  {Jaffe} T.~R.,  {G{\'o}rski}
  K.~M.,   {Davis} R.~J.,  2006, \mn@doi [\mnras]
  {10.1111/j.1365-2966.2006.10572.x}, \href
  {http://adsabs.harvard.edu/abs/2006MNRAS.370.1125D} {370, 1125}

\bibitem[\protect\citeauthoryear{{Dickinson} et~al.,}{{Dickinson}
  et~al.}{2009}]{dic09}
{Dickinson} C.,  et~al., 2009, \mn@doi [\apj] {10.1088/0004-637X/690/2/1585},
  \href {http://adsabs.harvard.edu/abs/2009ApJ...690.1585D} {690, 1585}

\bibitem[\protect\citeauthoryear{{Draine} \& {Lazarian}}{{Draine} \&
  {Lazarian}}{1998a}]{dra98}
{Draine} B.~T.,  {Lazarian} A.,  1998a, \mn@doi [\apjl] {10.1086/311167}, \href
  {http://adsabs.harvard.edu/abs/1998ApJ...494L..19D} {494, L19+}

\bibitem[\protect\citeauthoryear{{Draine} \& {Lazarian}}{{Draine} \&
  {Lazarian}}{1998b}]{dl98b}
{Draine} B.~T.,  {Lazarian} A.,  1998b, \mn@doi [\apj] {10.1086/306387}, \href
  {http://adsabs.harvard.edu/abs/1998ApJ...508..157D} {508, 157}

\bibitem[\protect\citeauthoryear{{Finkbeiner}}{{Finkbeiner}}{2004}]{fin04}
{Finkbeiner} D.~P.,  2004, \mn@doi [\apj] {10.1086/423482}, \href
  {http://adsabs.harvard.edu/abs/2004ApJ...614..186F} {614, 186}

\bibitem[\protect\citeauthoryear{{Habart}, {Boulanger}, {Verstraete}, {Pineau
  des For{\^e}ts}, {Falgarone}  \& {Abergel}}{{Habart}
  et~al.}{2003}]{2003A&A...397..623H}
{Habart} E.,  {Boulanger} F.,  {Verstraete} L.,  {Pineau des For{\^e}ts} G.,
  {Falgarone} E.,   {Abergel} A.,  2003, \mn@doi [\aap]
  {10.1051/0004-6361:20021489}, \href
  {http://adsabs.harvard.edu/abs/2003A%26A...397..623H} {397, 623}

\bibitem[\protect\citeauthoryear{{Kogut}, {Banday}, {Bennett}, {Gorski},
  {Hinshaw}  \& {Reach}}{{Kogut} et~al.}{1996}]{kog96}
{Kogut} A.,  {Banday} A.~J.,  {Bennett} C.~L.,  {Gorski} K.~M.,  {Hinshaw} G.,
   {Reach} W.~T.,  1996, \mn@doi [\apj] {10.1086/176947}, \href
  {http://adsabs.harvard.edu/abs/1996ApJ...460....1K} {460, 1}

\bibitem[\protect\citeauthoryear{{Leitch}, {Readhead}, {Pearson}  \&
  {Myers}}{{Leitch} et~al.}{1997}]{lei97}
{Leitch} E.~M.,  {Readhead} A.~C.~S.,  {Pearson} T.~J.,   {Myers} S.~T.,  1997,
  \mn@doi [\apjl] {10.1086/310823}, \href
  {http://adsabs.harvard.edu/abs/1997ApJ...486L..23L} {486, L23+}

\bibitem[\protect\citeauthoryear{{Liseau} et~al.,}{{Liseau}
  et~al.}{1999}]{1999A&A...344..342L}
{Liseau} R.,  et~al., 1999, \aap, \href
  {http://adsabs.harvard.edu/abs/1999A%26A...344..342L} {344, 342}

\bibitem[\protect\citeauthoryear{{Mathis}, {Mezger}  \& {Panagia}}{{Mathis}
  et~al.}{1983}]{mat83}
{Mathis} J.~S.,  {Mezger} P.~G.,   {Panagia} N.,  1983, \aap, \href
  {http://adsabs.harvard.edu/abs/1983A%26A...128..212M} {128, 212}

\bibitem[\protect\citeauthoryear{{Ridge} et~al.,}{{Ridge} et~al.}{2006}]{rid06}
{Ridge} N.~A.,  et~al., 2006, \mn@doi [\aj] {10.1086/503704}, \href
  {http://adsabs.harvard.edu/abs/2006AJ....131.2921R} {131, 2921}

\bibitem[\protect\citeauthoryear{{Watson}, {Rebolo},
  {Rubi{\~n}o-Mart{\'{\i}}n}, {Hildebrandt}, {Guti{\'e}rrez},
  {Fern{\'a}ndez-Cerezo}, {Hoyland}  \& {Battistelli}}{{Watson}
  et~al.}{2005}]{wat05}
{Watson} R.~A.,  {Rebolo} R.,  {Rubi{\~n}o-Mart{\'{\i}}n} J.~A.,  {Hildebrandt}
  S.,  {Guti{\'e}rrez} C.~M.,  {Fern{\'a}ndez-Cerezo} S.,  {Hoyland} R.~J.,
  {Battistelli} E.~S.,  2005, \mn@doi [\apjl] {10.1086/430519}, \href
  {http://adsabs.harvard.edu/abs/2005ApJ...624L..89W} {624, L89}

\makeatother
\end{thebibliography}
