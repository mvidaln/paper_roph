% mnras_template.tex
%
% LaTeX template for creating an MNRAS paper
%
% v3.0 released 14 May 2015
% (version numbers match those of mnras.cls)
%
% Copyright (C) Royal Astronomical Society 2015
% Authors:
% Keith T. Smith (Royal Astronomical Society)

% Change log
%
% v3.0 May 2015
%    Renamed to match the new package name
%    Version number matches mnras.cls
%    A few minor tweaks to wording
% v1.0 September 2013
%    Beta testing only - never publicly released
%    First version: a simple (ish) template for creating an MNRAS paper

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Basic setup. Most papers should leave these options alone.
\documentclass[a4paper,fleqn,usenatbib]{mnras}

% MNRAS is set in Times font. If you don't have this installed (most LaTeX
% installations will be fine) or prefer the old Computer Modern fonts, comment
% out the following line
\usepackage{newtxtext,newtxmath}
% Depending on your LaTeX fonts installation, you might get better results with one of these:
%\usepackage{mathptmx}
%\usepackage{txfonts}

% Use vector fonts, so it zooms properly in on-screen viewing software
% Don't change these lines unless you know what you are doing
\usepackage[T1]{fontenc}
\usepackage{ae,aecompl}


%%%%% AUTHORS - PLACE YOUR OWN PACKAGES HERE %%%%%

% Only include extra packages if you really need them. Common packages are:
\usepackage{graphicx}	% Including figure files
\usepackage{amsmath}	% Advanced maths commands
\usepackage{amssymb}	% Extra maths symbols

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%% AUTHORS - PLACE YOUR OWN COMMANDS HERE %%%%%

% Please keep new commands to a minimum, and use \newcommand not \def to avoid
% overwriting existing commands. Example:
%\newcommand{\pcm}{\,cm$^{-2}$}	% per cm-squared

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%% TITLE PAGE %%%%%%%%%%%%%%%%%%%

% Title of the paper, and the short title which is used in the headers.
% Keep the title short and informative.

\title[Centimeter-wave continuum radiation from $\rho$~Oph]{Resolved
  observations of the centimeter-wavelength continuum from the
  $\rho$~Ophiuci molecular cloud, I: Local emissivity boost in the
  $\rho$~Oph~W photo-dissociation-region}

\author[Casassus et al.]{
  Simon Casassus$^{1}$\thanks{E-mail: simon@das.uchile.cl}
  ....
  \\
  $^{1}$Departamento de Astronom\'{\i}a, Universidad de Chile, Casilla 36-D, Santiago, Chile\\
}
%  Glenn J. White$^{2,3}$, 
%  Jamie Stevens,
%  Michael Burton$^6$,  \newauthor  
%  Clive Dickinson$^{2}$, 
%  Kieran Cleary$^{3}$, 
%%  Roberta Paladini$^{2}$,  \newauthor
%  $^{1}$  Departamento de Astronom\'{\i}a, Universidad de Chile, Casilla 36-D,
%  Santiago, Chile\\ 
%  $^{2}$ Infrared Processing and Analysis Center, California Institute of Technology, M/S 220-6,  
%1200 E. California Blvd., Pasadena, CA 91125. \\
%  $^{3}$ Chajnantor Observatory, M/S 105-24,  California Institute of Technology, Pasadena, CA 91125 \\
%  $^{4}$ Department of Physics and Astronomy, The Open University, Milton Keynes MK7 6AA, UK\\
%  $^{5}$ The Rutherford Appleton Laboratory, Didcot, Oxfordshire OX11 0QX, UK. \\
%  $^{6}$ School of Physics, University of New South Wales, Sydney NSW 2052, Australia\\




% These dates will be filled out by the publisher
\date{Accepted XXX. Received YYY; in original form ZZZ}

% Enter the current year, for the copyright statements etc.
\pubyear{2017}

\begin{document}
\label{firstpage}
\pagerange{\pageref{firstpage}--\pageref{lastpage}}
\maketitle



\label{firstpage}

\begin{abstract}
\begin{itemize}
\item The CBI2 mosaic of $\rho$~Oph confirms the CBI1 morphology, and
  highlights the complete absence of detectable 31~GHz signal from
  S~1, the brightest IR nebula in the complex. The spinning dust
  emissivity per nucleon varies by two-orders of magnitudes.
\end{itemize}
\end{abstract}

%The main source of
%residual charge in $\rho$~Oph~W is the photoionization of carbon by UV
%photons above 11.3~eV.

\begin{keywords}
radiation mechanisms: general, radio continuum: general ISM,  sub-millimetre,
ISM: clouds,
\end{keywords}



\section{Introduction}

Since 1996, experiments designed to measure CMB anisotropy have found
an anomalous diffuse foreground in the range of 10-90~GHz. This
diffuse emission is correlated with thermal emission from dust grains
at 100~$\mu$m on large angular scales, and at high galactic latitudes.
The spectral index (considering $S_\nu \propto \nu^\alpha$) of the
radio-IR correlated signal is $\alpha_\mathrm{radio/IR} \sim 0$ in the
range 15-30~GHz, and so reminiscent of optically thin free-free
\citep{kog96}. But $\alpha_\mathrm{radio/IR} \sim -0.85$ between
20-40~GHz, for high-latitude cirrus \citep{dav06}. The absence of
H$\alpha$ emission concomitant to radio free-free, would require an
electron temperature $T_e \geqslant 10^6$ to quench H\,{\sc i}
recombination lines \citep{lei97}.


Another possibility was presented by \citet{dra98}. They calculated
that spinning interstellar very small dust grains produce rotational
dipole emission in the range from 10 to 100 GHz, at levels that could
account for the excess diffuse foreground observed over the free-free
component.


Observations of specific targets may shed light on the anomalous
foreground, whose existence in the diffuse ISM is inferred
statistically. Cm-wavelength radiation well in excess of the predicted
levels from free-free, synchrotron and Rayleigh-Jeans dust emission,
has been found in a dozen clouds \citep[][]{fin04, wat05,
  2006ApJ...639..951C, 2008MNRAS.391.1075C, 2009MNRAS.394L..46A,
  dic09, 2011MNRAS.414.2424V}. The observed spectral energy distributions (SEDs) are
a qualitative match to spinning dust models.


In particular $\rho$~Oph~W, the region of the $\rho$~Oph molecular
cloud exposed to UV radiation from HD~147889, is among the nearest
example of a photo-dissociation-region (PDR) in a star forming region,
at a distance of 135~pc.  It is seen edge-on and extends over
10$\times$3~arcmins. $\rho$~Oph is a region of intermediate-mass star
formation. It does not host a conspicuous H\,{\sc ii} region, by
contrast to the Orion Bar, another well studied PDR, where UV fields
are 100 times stronger. $\rho$~Oph~W has been extensively studied in
the far-IR atomic lines observed by ISO
\citep[][]{1999A&A...344..342L, 2003A&A...397..623H}. Observations of
the bulk molecular gas in $\rho$~Oph, through $^{12}$CO(1-0) and
$^{13}$CO(1-0), are publicly available from the COMPLETE database
\citep[][]{rid06}.


Cosmic Background Imager (CBI) observations showed that the
suprisingly bright cm-wave continuum from $\rho$~Oph, for a total {\em
  WMAP}~33~GHz flux density of $\sim$20~Jy, stems from $\rho$~Oph~W
and is fit by spinning dust models \citep[][]{cas08}. However, the
peak at all IR wavelengths, i.e. the circumstellar nebula around S~1,
is undetectable in the CBI data. Upper limits on S~1 flux density and
a cross-correlation with {\em Spitzer}-IRAC~8$\mu$m rule out a linear
radio/IR relationship within the CBI 45~arcmin primary beam (covering
the bulk $\rho$~Oph by mass).




Dust emissivities in the IR are roughly proportional to the energy
density of UV radiation: $I_\mathrm{IR} \propto U N_d$, where
$U$\footnote{ The dimensionless parameter $U$ measures the average
  intensity of radiation in 4$\pi$~sr, integrated from 0.09 to
  8~$\mu$, in units of the corresponding intensity in the solar
  neighbourhood \citep{mat83}} measures the dust-heating radiation
field, and $N_d$ is the column of IR emitters.  A linear relationship,
$I_\mathrm{cm} \propto N_d$ could be proposed, inspired from the
radio-IR correlated signal in Cirrus clouds. The universality of
the spinning dust emissivity per dust grain is marginally consistent
with the CBI data. 


Here we report on CBI2 observations of $\rho$~Oph. We confirm that
$I_\mathrm{cm} \propto N_d$ breaks down on scales smaller than 1~deg,
and that there are strong cm-wavelength emissivity variations in
$\rho$~Oph. In the framework of the spinning dust interpretation,
these emissivity variations may hold the key to identify the dominant
grain spin-up mechanisms. The structure of this article is as follows:
Sec.~\ref{sec:cbiobs} describes the CBI2 observations,
Sec.~\ref{sec:discussion} analyses the data, and Sec.~\ref{sec:conc}
concludes.


\section{CBI2 Observations} \label{sec:cbiobs}

Pending: log of observations****. 

The CBI2 MEM mosaic shown on Fig.~\ref{fig:globalrgb} confirms the
intriguing absence of any detectable 31~GHz signal from S~1, the
brightest IR nebula in $\rho$~Oph. Relative to $\rho$~Oph~W, S~1 is
too faint at 31~GHz for a constant spinning dust emissivity per
H-nucleus. As described in \citep[][]{cas08}, the ratio
\[
\Rightarrow   R = G_\circ \times  I_\nu(31\mathrm{GHz})
/I(\mathrm{PAH}~11.3~\mu\mathrm{m})  
\]
should be constant for a constant spinning dust emissivity per
H-nucleus.  The spinning dust emissivities are remarkably independent
of environment \citep[varying at most a factor of 10 over 4 orders of
  magnitude in UV density*** update***][]{dl98b}, while the PAH
intensities are $\propto G_\circ$ (the local UV field).  

Given {\em Spitzer}~IRS spectroscopy of the 11.3~$\mu$m PAH band,
$G_\circ$ from {\em ISO}, and a 3~$\sigma$ upper limit for S~1 from
the CBI2 mosaic, we find that $R$ is at least 42 times greater in
$\rho$~Oph~W than in S~1. 

Therefore environmental factors boost the spinning dust emissivities
in $\rho$~Oph~W, and quench it in S~1. 


\begin{figure}
\caption{\label{fig:globalrgb} Overview of the $\rho$~Oph cloud. Red:
  MIPS~24~$\mu$m, green: IRAC~8~$\mu$m, blue: processed 2MASS~Ks. The
  circles indicate the locations of the 3 earliest stars in the field
  - HD147889 is the westernmost. S~1 lies towards the East, and SR~3
  towards the South.  Countours follow an MEM model of the 31~GHz
  continuum measured by CBI2, and the black and white rectangles are
  the available Spitzer apertures.  $x-$ and $y-$ axis are offset
  J2000 RA and DEC, in degrees of arc.  }
\begin{center}
\includegraphics[width=0.45\textwidth,height=!]{multi_roph_S1_SR3_rgb_CBI2.pdf}
\end{center}
\end{figure}

\section{Local excitation of spinning dust} \label{sec:local}


Local excitation of grain rotation to $\sim$30~GHz frequencies may be
due to radiative torques, increasing with UV, parametrised with
in units of the  intensity from  $G_\circ$ ******

The exciting star of $\rho$~Oph~W, HD147889, is located towards the SW
in Fig.~\ref{fig:globalrgb}, as reflected in the layered structure of the
IR tracers, as that of an IR rainbow. Deeper into the molecular core,
the UV radiation field from HD147889 is attenuated, and the emergent
continuum progressively shifts into the far-IR.


Mid-IR dust emission, due to VSG fluorescence, is proportional to the
local UV field: $I_\mathrm{IR} \propto U N_\mathrm{VSG}$, where
$N_\mathrm{VSG}$ stands for the column of VSGs.  Spinning dust is
proportional to the column of VSG, and theoretical models show it is
fairly independent of the local UV field. If there were no further
environmental factors on the spinning dust emissivity, then the radio
signal should reach much deeper into the $\rho$~Oph~W PDR - it should
at least follow MIPS~24~$\mu$m.

Additionally \citet{cas08} have shown that the brightest IR nebulae in
$\rho$~Oph have no radio counterpart. The absence of radio sources
coincident with the IR-bright circumstellar dust about S~1 and SR~3
can only marginally be reconciled with VSG depletion. 

Another explanation, also suggested from the calculations by
\cite{dra98} ****PENDING: theoretical results from SPDUST model*****,
is that the dominant source of rotational excitation is ``plasma
drag'', due to the interaction of the grain dipoles with passing ions
- namely C$^+$ in the context of PDRs. If so the spinning dust
emissivities would be best understood in terms of an emission measure,
rather than VSG column: $I_\mathrm{cm} \propto N_\mathrm{VSG}
N(\mathrm{C}+)$. 

The absence of S~1 and SR~3 in the radio maps of $\rho$~Oph is then
naturally explained by the fact that these stars are too cold to
create conspicuous C\,{\sc ii} regions \citep[][]{cas08}. 











\appendix 



\section[]{Image reconstruction} \label{sec:appendix}

The traditional image-reconstruction algorithm `CLEAN' is not well
suited for extended sources. Using the Miriad task `clean' we found
that the CLEAN models gave large residuals, with an intensity
amplitude much greater than that expected from thermal noise, and with
a spatial structure reflecting the convolution of the negative
synthetic side-lobes with the morphology of the source. Attempts to
improve dynamic range using Miriad's `maxen' task gave worse
results. We therefore attempted to design a special-purpose image
reconstruction algorithm, based on sky-plane deconvolution, which we
describe in this appendix.


To obtain a model sky image that fits the data we need to solve the
usual deconvolution problem, i.e. obtain the model image $I^m$ 
that minimize a merit function $L$: 
\begin{equation}
L = \sum_i \left[ \left( I_i - I^m A_i  \times B_i  \right)^2 w_i   \right] + S(I^m),
\end{equation}
where the sum extends over the number of fields, $A_i$ is the primary
beam attenuation, $B_i$ is the synthetic beam, $I_i$ the dirty map,
$w_i = 1 / \sigma_i^2$, with $\sigma_i$ the noise map as calculated
with the `sensitivity' option to Miriad's task `invert', and $S(I^m)$
is a regularizing term. After several attempts with a variety of
functional forms for $S$, we found that we obtained best results with
a pure $\chi^2$ optimization, i.e., with $S = 0$. This choice results
in models that fit the noise, which we have to bear in mind for the
subsequent analysis. Trials with a mock dataset simulated on the {\em
  IRAC}~8~$\mu$m image of $\rho$~Oph, with identical $uv$-plane
coverage as the CABB observations, showed that we obtain best results
with pure $\chi^2$ reconstructions (the optimal $I_m$ approximates
best the input {\em IRAC}~8~$\mu$m image, in a least-squares sense,
for a variety of initial conditions). 

It is well known that incomplete sampling in the $uv$-plane, and in
particular the lack of total power measurements, leads to flux
loss. However, if a good template of the image exists, then the
missing fourier components can be approximately recovered in $I^m$. 












\bibliographystyle{mnras}
\bibliography{references_CBI2.bib}
	

%\bibitem[Vidal et al. (2010)]{vid10} Vidal, M., Casassus, S.,
%  Dickinson, C., Witt, A., Castellanos, P., Paladini, R., Cleary, K.,
%  Davis, R., Davies, R., Taylor, A., 2010, MNRAS, {\em in prep} 

%\end{thebibliography}

\bsp
\label{lastpage}

\end{document}
