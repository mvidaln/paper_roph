
\documentclass[11pt]{article}

\usepackage[authoryear]{natbib}  % added by simon 3/7/08

\usepackage{epsfig,graphicx,graphics}

\setlength{\oddsidemargin}{5mm}
\setlength{\evensidemargin}{5mm}
\setlength{\topmargin}{-0.2in}
\setlength{\footskip}{0.4in}
\setlength{\textheight}{24cm}
\setlength{\textwidth}{16cm}

\setlength{\headheight}{0.5in}
\setlength{\headsep}{0.2in}



\begin{document}

\begin{center}{\Large \bf  Motivations for carbon RRL searches in PDRs from radio
  observations of $\rho$~Oph~W}
\end{center}




\section{Introduction}

Since 1996, experiments designed to measure CMB anisotropy have found
an anomalous diffuse foreground in the range of 10-90~GHz. This
diffuse emission is correlated with thermal emission from dust grains
at 100 $\mu$m on large angular scales, and high galactic
latitudes. The spectral index (considering $S_\nu \propto \nu^\alpha$)
of the radio-IR correlated signal is $\alpha_\mathrm{radio/IR} \sim 0$
in the range 15-30~GHz, as optically thin free-free \citep{kog96}. But
$\alpha_\mathrm{radio/IR} \sim -0.85$ between 20-40~GHz, for
high-latitude cirrus \citep{dav06}. The absence of H$\alpha$ emission
concomitant to radio free-free, would require an electron temperature
$T_e > 10^6$ to quench H\,{\sc i} recombination lines
\citep{lei97}. 


Another possibility was presented by \citet{dra98}. They calculated
that spinning interstellar very small dust grains produce rotational
dipole emission in the range from 10 to 100 GHz, at levels that could
account for the excess diffuse foreground observed over the free-free
component.


Observations of specific targets may shed light on the anomalous
foreground, whose existence in the diffuse ISM is inferred
statistically. Anomalous cm-wavelength radiation has been found in
about a dozen clouds \citep[][]{fin04, wat05, cas06, cas08, sca09,
  dic09, vid10}.  A common feature of all cm-bright clouds is that
they host conspicuous PDRs. The cm-wavelength continuum holds the
potential of a new PDR diagnostic.



$\rho$~Oph~W, the region of the $\rho$~Oph molecular cloud exposed to
UV radiation from HD147889, is among the nearest example of a
photo-dissociation-region (PDR) in a star forming region, at a
distance of 135~pc.  It is seen edge-on and extends over
10$\times$3~arcmins. $\rho$~Oph is a region of intermediate-mass star
formation. It does not host a conspicuous H\,{\sc ii} region, by
contrast to the Orion Bar, another well studied PDR, where UV fields
are 100 times stronger. $\rho$~Oph~W has been extensively studied in
the far-IR atomic lines observed by ISO (Liseau et al. 1999, A\&A,
344, 342; Habart et al., 2003, A\&A, 397, 623). Observations of the
bulk molecular gas in Oph~A, through $^{12}$CO(1-0) and
$^{13}$CO(1-0), are publicly available from the COMPLETE database
(from FCRAO~14m, with a 23~arcsec beam, Ridge et al. 2006, AJ,
131, 2921).




\section{CABB Observations} \label{sec:cbiobs}



The CABB mosaic of $\rho$~Oph~W is shown on Fig.~\ref{fig:ATCArgb}. It
is a linear primary-beam-weighted average of 6 pointings in H~75
configuration. Each field was individually restored using CLEAN.




\begin{figure}
\begin{center}
\includegraphics[width=0.8\columnwidth,height=!]{multi_roph_S1_SR3_rgb.pdf}
% image_roph_CBI_1.pl
\end{center}
\caption{\label{fig:ATCArgb} Restored mosaic of six ATCA H75 pointings
  in $\rho$~Oph~W, and comparison with IR tracers.  $x-$ and $y-$axis
  show offset RA and DEC from $\rho$~Oph~W (J2000 16h25m57s,
  -24d20m50s), in degrees of arc. {\bf Blue contours}: ATCA H75
  mosaic, over 2~GHz bandwith, and centred on 20160~MHz. Contour
  levels are at 0.0002, 0.0003, 0.0006, 0.0008, 0.001~Jy~beam$^{-1}$.
  {\bf White contours}: MEM model of CBI visibilities, at 31~GHz.  The
  contour levels are at 0.067 ,0.107 ,0.140 ,0.170 and 0.197, in
  MJy~sr$^{-1}$. {\bf Red:} MIPS~24~$\mu$m. {\bf Green:}
  IRAC~8~$\mu$m. {\bf Blue:} 2MASS~Ks band. 
  }
\end{figure}


\begin{table}
\caption{Log of observations. We give the centred frequency of both
  CABB correlator IFs setting. Each IF counts 2048 channels. The
  spectral resolution is 1~MHz.}
\label{table:log}
\begin{center}
\begin{tabular}{llllllll}
\hline 
11-May-2009    &   H~168$^{a}$  &  17481$^{b}$   & 20160$^{c}$  & 6 \\ 
12-May-2009    &   H~168        &  5500          & 8800         & 1\\ 
07-Jul-2009    &   H~75         &  17481         & 20160        & 6\\ 
08-Jul-2009    &   H~75         &  33157         & 39157        & 6\\ \hline 
\end{tabular}
\end{center}
\begin{flushleft}
$^a$ ATCA array configuration. The length of the shortest baseline, in
  metres, is indicated after the symbol ``H''.  \\
$^b,^c$ Centre frequencies of each CABB IFs.. \\
$^d$ Number of fields in $\rho~$Oph~W.
\end{flushleft}
\end{table}


\section{Local excitation of spinning dust} \label{sec:cbiobs}

The exciting star of $\rho$~Oph~W, HD147889, is located towards the SW
in Fig.~\ref{fig:ATCArgb}, as reflected in the layered structure of
the IR tracers, as that of an IR rainbow. Deeper into the molecular
core, the UV radiation field from HD147889 is attenuated, and the
emergent continuum progressively shifts into the far-IR. 


Mid-IR dust emission, due to VSG fluorescence, is proportional to the
local UV field: $I_\mathrm{IR} \propto U N_\mathrm{VSG}$, where
$N_\mathrm{VSG}$ stands for the column of VSGs.  Spinning dust is
proportional to the column of VSG, and theoretical models show it is
fairly independent of the local UV field (e.g. \citet{yv09}). If there
were no further environmental factors on the spinning dust emissivity,
then the radio signal should reach much deeper into the $\rho$~Oph~W
PDR - it should at least follow MIPS~24~$\mu$m. 

Additionally \citet{cas08} have shown that the brightest IR nebulae in
$\rho$~Oph have no radio counterpart. The absence of radio sources
coincident with the IR-bright circumstellar dust about S~1 and SR~3
can only marginally be reconciled with VSG depletion. 

Another explanation, also suggested from the calculations by
\cite{dra98} ****PENDING: theoretical results from SPDUST model*****,
is that the dominant source of rotational excitation is ``plasma
drag'', due to the interaction of the grain dipoles with passing ions
- namely C$^+$ in the context of PDRs. If so the spinning dust
emissivities would be best understood in terms of an emission measure,
rather than VSG column: $I_\mathrm{cm} \propto N_\mathrm{VSG}
N(\mathrm{C}+)$. 

The absence of S~1 and SR~3 in the radio maps of $\rho$~Oph is then
naturally explained by the fact that these stars are too cold to
create conspicuous C\,{\sc ii} regions \citep[][]{cas08}. 


\section{carbon RRL search} \label{sec:crrls}

We searched for carbon RRLs in a 2~GHz bandwith centred on
17481~MHz. Three $\alpha$-type RRLs fall into the 17481~GHz IF:
C71$\alpha$ 18.00153, C72$\alpha$ 17.26682, C73$\alpha$
16.57156. 

No carbon RRLs are detected. In the 17481~GHz IF the velocity width of
each channel is $\sim$16~km~s$^{-1}$. The noise in single-channel
reconstructions is $\sim$2~mJy~beam$^{-1}$, for a 30~arcsec beam
FWHM. The systemic velocity of $\rho$~Oph~W is $V_\mathrm{lsr} =
+3~$km~s$^{-1}$ \citep{bro74a,pan78}. 



\section{Rough estimates of  carbon RRL intensities in $\rho$~Oph~W} \label{sec:crrlsprev}


We take a depth of 0.04~pc, which at a distance of 135~pc subtends
1~arcmin, an ionisation fraction of $10^{-4}$, due to carbon
photoionisation, $T_e = 100~$K, and a H-nucleus density of
$10^5$~cm$^{-3}$  (among values reported previously for
$\rho$~Oph~W).  


The peak intensity of the emergent C71$\alpha$ is 18~mJy~beam$^{-1}$,
for LTE, with a 1.5~km~s$^{-1}$ FWHM, and a 30~arcsec beam.  When
diluted in the $\sim$~16~km~s$^{-1}$ channels of CABB, the expected
signal drops down to $\sim$~1~mJy~beam$^{-1}$. For $Te < 100~$K, the
LTE deviations become important, so that $n=72$ is depopulated
relative to LT (i.e. $b < 1$).  

 


\section{Previous carbon RRL searches} \label{sec:crrlsprev}

\citet{pan78} examined the most complete set of RRL data towards
$\rho$~Oph to date. The line profiles observed at lower frequencies
have widths of 1.5~km~s$^{-1}$ FWHM.  They mapped the neighbourhood of
S~1, but did not extend their coverage to $\rho$~Oph~W,
unfortunately. The highest frequency RRLs considered by \citet{pan78}
are C90$\alpha$ and C91$\alpha$, at $\sim$9~GHz, which they
interpreted as stemming from circumstellar gas about S~1, with
electron densities $n_e \sim 15~$cm$^{-3}$ and $T_e \sim 150~$K. This
circumstellar C\,{\sc ii} region was inferred to be less than
$\sim$2~arcmin in diameter, and surrounded by a diffuse halo with $n_e
\sim 1~$cm$^{-3}$, traced by the lower frequency carbon RRLs.



\citet{cas08} searched for carbon RRLs from $\rho$~Oph~W using MOPRA,
equipped with a similar correlator as CABB.  Their 3~$\sigma$ upper
limit of 1.1~MJy~sr$^{-1}$ on C73$\alpha$, diluted in 5~arcmin pixels,
corresponds to 73~mJy~beam$^{-1}$ in a 30~arcsec beam. 


\section{Potential targets for carbon RRL searches in cm-bright
  sources}

We need resolved targets for a morphological analysis, coupling radio
continuum, IR continuum, and carbon RRLs. 

North: LDN~1622 (Casassus et al. 2006), M~78 (see Castellanos et
al. 2010, in prep), and selected hot spots in Perseus, see Tibbs et
al. (2009, arXiv:0909.4682).\\ 



South: $\rho$~Oph~W. 














\begin{thebibliography}{99}

%\bibitem[\protect\citeauthoryear{Andr\'e et al. }{1988}]{and88}
%  Andr\'e, P., Montmerle, T., Feigelson, E. D., Stine, P. C., \&
%  Klein, K.-L. 1988, ApJ, 335, 940
%
%
%\bibitem[\protect\citeauthoryear{Abergel et al.}{1996}]{abe96} Abergel, A., et al., 1996, A\&A, 315, L329. 
%
%\bibitem[\protect\citeauthoryear{Baart et al.}{1980}]{baa80} Baart,
%  E.E., de Jager, G., Mountfort, P.I., A\&A, 1980, 92, 156.
%
%%\bibitem[\protect\citeauthoryear{Baker \& Menzel}{1938}]{bak38} Baker,
%%  J.G., Menzel, D.H., 1938, ApJ, 88, 52
%
%\bibitem[\protect\citeauthoryear{Barsony et al.}{1997}]{bar97}
%  Barsony, M., Kenyon, S. J., Lada, E. A., \& Teuben, P. J. 1997, ApJS,
%  112, 109 
%
%\bibitem[\protect\citeauthoryear{Behrend \& Maeder}{2001}]{beh01}
%  Behrend, R., Maeder, A., 2001, A\&A, 373, 190
%
%\bibitem[\protect\citeauthoryear{Bennett}{2003}]{ben03}
%	Bennett, C. L., Halpern, M., Hinshaw, G., Jarosik, N., Kogut,
%        A., Limon, M., Meyer, S. S., Page, L., Spergel, D. N., Tucker,
%        G. S., Wollack, E., Wright, E. L., Barnes, C., Greason, M. R.,
%        Hill, R. S., Komatsu, E., Nolta, M. R., Odegard, N., Peiris,
%        H. V., Verde, L., Weiland, J. L., 2003, ApJSS, 148, 1
%
%\bibitem[\protect\citeauthoryear{Bernard  et al.}{1993}]{ber93}
%  Bernard, J.P., Boulanger, F., Puget, J.L., 1993, A\&A 277, 609.
%
%\bibitem[\protect\citeauthoryear{Bern\'e  et al.}{2007}]{ber07} Bern\'e et al. 2007, A\&A, 469, 575
%
%
%\bibitem[\protect\citeauthoryear{Black \& van Dishoeck}{1987}]{bla87}
%  Black, J.H., van Dishoeck, E.F., 1987, Apj, 322, 412
%
%\bibitem[\protect\citeauthoryear{Bontemps et al.}{2001}]{bon01}
%  Bontemps, S., et al., A\&A, 372, 173.
%
%
%
%\bibitem[\protect\citeauthoryear{Boulanger \& Perault}{1988}]{bou88}
%  Boulanger, F., Perault, M., 1988, ApJ, 330, 964
%
%\bibitem[\protect\citeauthoryear{Boulanger et al.}{1996}]{bou96}
%  Boulanger, F., et al., 1996, A\&A, 315, L325. % 16 coauthors
%
%\bibitem[\protect\citeauthoryear{Brown et al.}{1974}]{bro74} Brown, R.L., Gammon, R.H., Knapp, G.R., Balick, B., 1974, ApJ, 192, 607. 
%
\bibitem[\protect\citeauthoryear{Brown \& Knapp}{1974}]{bro74a} Brown,  R.L., Knapp, G.R., 1974, ApJ, 189, 253
%
%%\bibitem[\protect\citeauthoryear{Brown \& Zuckerman}{1975}]{bro75}   Brown, R.L., Zuckerman, B., 1975, ApJL, 202, 125
%
%
%\bibitem[\protect\citeauthoryear{Calabretta}{1991}]{cal91} Calabretta, M.R., 1991, AuJPh, 44, 441 
%
%\bibitem[\protect\citeauthoryear{Casassus et al.}{2004}]{cas04}
%  Casassus, S., Readhead, A.C.S., Pearson, T.J., Nyman, L.-\AA,
%  Shepherd, M.C., Bronfman, L., 2004, ApJ, 603, 599
%
%
%
\bibitem[\protect\citeauthoryear{Casassus et al.}{2006}]{cas06} Casassus, S.,
  Cabrera, G. F., F\"orster, F., Pearson, T. J., Readhead, A. C. S.,
  Dickinson, C., 2006, ApJ, 639, 951

%\bibitem[\protect\citeauthoryear{Casassus et al.}{2007}]{cas07}
%  Casassus, S., Nyman, L.-\AA., Dickinson, C., Pearson, T. J., 2007,
%  MNRAS, 382, 1607

\bibitem[\protect\citeauthoryear{Casassus et al.}{2008}]{cas08}
  Casassus et al. 2008, MNRAS, 391, 1075


%\bibitem[\protect\citeauthoryear{Cartwright et al.}{2005}]{car05}
%  Cartwright, J. K., Pearson, T. J., Readhead, A. C. S., Shepherd,
%  M. C., Sievers, J. L., Taylor, G. B., 2005, ApJ, 623, 11
%
%\bibitem[\protect\citeauthoryear{Castelli}{2003}]{cas03} Castelli, F.,
%  Kurucz, R. L., 2003, IAU symp. 210, A20.
%
%\bibitem[\protect\citeauthoryear{Chan}{2001}]{cha01} Chan, S. J., et
%  al., 2001, in 'The Calibration Legacy of the ISO
%  Mission'. Eds. L. Metcalfe \& M.F. Kessler, ESA SP-481
%
%\bibitem[\protect\citeauthoryear{Chrysostomou et al.}{1996}]{chr96}
%  Chrysostomou, A., Clark, S. G., Hough, J. H., Gledhill, T. M., McCall, A., Tamura, M., 1996, MNRAS,
%  278, 449
%
%
%
%\bibitem[\protect\citeauthoryear{Condon el al.}{1993}]{co93} Condon,
%  J.J., Griffith, M.R. \& Wright, A.E., 1993, AJ, 106, 1095
%

\bibitem[Davies et al.  (2006)]{dav06}
  Davies, R. D., Dickinson, C., Banday, A. J., Jaffe, T. R., G\'orski,
  K. M., Davis, R. J., 2006, MNRAS, 370, 1125. 

% 
%\bibitem[\protect\citeauthoryear{Dickinson et al.}{2006}]{dic06}
%  Dickinson C., Casassus S., Pineda J. L., Pearson T. J., Readhead
%  A. C. S., Davies R. D., 2006, ApJ, 643, L111
%
%\bibitem[\protect\citeauthoryear{Dickinson et al.}{2003}]{dic03}
%  Dickinson, C., Davies, R. D.; Davis, R. J., 2003, MNRAS, 341, 369
%

\bibitem[Dickinson et al. (2009)]{dic09}
  Dickinson, C., Davies, R. D., Allison, J. R.k, Bond, J. R.,  Casassus,
  S., Cleary, K., Davis, R. J., Jones, M. E., Mason, B. S., Myers,
  S. T., and 7 coauthors, ApJ, 690, 1585.


%\bibitem[\protect\citeauthoryear{Draine  et al.}{2003}]{dra03} Draine, B., 2003, ARAA, 41, 241
%
%\bibitem[\protect\citeauthoryear{Draine \& Bertoldi}{1996}]{dra96}
%  Draine, B. T., Bertoldi, F., 1996, ApJ, 468, 269.
%

\bibitem[\protect\citeauthoryear{Draine \& Lazarian}{1998a}]{dra98} Draine, B.T., Lazarian, A.,  1998a, ApJL, 494, L19

\bibitem[\protect\citeauthoryear{Draine \& Lazarian}{1998b}]{dl98b} Draine, B.T., Lazarian, A.,  1998b, ApJ, 508, 157

%\bibitem[\protect\citeauthoryear{Draine \& Lazarian}{1999}]{dl99}
%  Draine, B.T., Lazarian, A.,  1999, ApJ 512, 740
%
%\bibitem[\protect\citeauthoryear{Draine \& Lazarian}{1999b}]{dra99b}
%  Draine, B.T., Lazarian, A.,  1999, 'Microwave Foregrounds', ASP
%  Conference Series, Vol. 181, A. de Oliveira-Costa and M. Tegmark,
%  eds. 
%
%\bibitem[\protect\citeauthoryear{Draine \& Li}{2001}]{dl01} Draine,
%  B.T., Li, A., 2001, ApJ, 551, 807
%
%\bibitem[\protect\citeauthoryear{Draine \& Li}{2007}]{dl07} Draine,
%  B.T., Li, A., 2007, ApJ, 657, 810 
%
%
%
%\bibitem[\protect\citeauthoryear{Dupac et al.}{2003}]{dup03} Dupac,
%  X., et al., 2003, A\&A, 404, L11.
%
%\bibitem[\protect\citeauthoryear{Elias}{1978}]{eli78} Elias, J.H., ApJ, 1978, 224, 453
%
%
%\bibitem[\protect\citeauthoryear{Encrenaz}{1974}]{enc74} Encrenaz,
%  P.J., 1974, ApJL, 189, 135
%
%\bibitem[\protect\citeauthoryear{Encrenaz}{1974}]{fal81} Falgarone,
%  E., Gilmore, W., 1981, A\&A, 95, 32 
%
%
%
%\bibitem[\protect\citeauthoryear{Ferland et al.}{1998}]{fer98}
%Ferland, G. J. Korista, K.T. Verner, D.A. Ferguson, J.W. Kingdon,
%J.B. Verner, \& E.M. 1998, PASP, 110, 761
%
%\bibitem[Finkbeiner et al.(2002)]{fin02} Finkbeiner, D.P., Schlegel, D.J.,
%Frank, C., Heiles, C., 2002, ApJ, 566, 898
%

\bibitem[\protect\citeauthoryear{Finkbeiner}{2004}]{fin04} Finkbeiner, D.P., 2004, ApJ, 614, 186, 
%
%\bibitem[\protect\citeauthoryear{Gagn\'e et al.}{2004}]{gag04} Gagn\'e, M.,  Skinner, S.L., Daniel, K. J., 2004, ApJ, 613, 393
%
%\bibitem[\protect\citeauthoryear{Garc\'ia Lario}{2001}]{gar01}
%  Garc\'ia Lario, P., 2001, SAI/2001-030/Rp. Version 1.0. 
%
%
%\bibitem[\protect\citeauthoryear{Gaustad et al.}{2001}]{gau01}
%Gaustad, J. E., McCullough, P. R., Rosing, W., Van Buren, D., 2001, PASP, 113, 1326
%
%\bibitem[\protect\citeauthoryear{Grasdalen et al.}{1973}]{gra73}
%  Grasdalen , G., Strom, K.M., Strom, S.E., 1973, ApJL, 184, 53
%
%\bibitem[\protect\citeauthoryear{Hafner \& Meyer}{1995}]{haf95} 
%Hafner, L.M., Meyer, D.M., 1995, ApJ, 453, 450
%
%
%\bibitem[\protect\citeauthoryear{Habart et al.}{2003}]{hab03} Habart, E.,
%  Boulanger, F., Verstraete, L., Pineau des For\^{e}ts, G., Falgarone,
%  E., Abergel, A., 2003, A\&A, 397, 623
%
%\bibitem[\protect\citeauthoryear{Heiles et al.}{1996}]{hei96} Heiles,
%  C., Bon-Chul, K., Levenson, N.A., Reach, W.T., 1996, ApJ, 462, 326
%
%\bibitem[\protect\citeauthoryear{Hinshaw et al.}{2007}]{hin07} Hinshaw, G., et al., 2007, ApJS, 170, 288
%
%\bibitem[\protect\citeauthoryear{Habing}{1968}]{hab68} Habing, H. J., 1968, Bull. Astr. Inst. Netherlands., 19, 421
%
%\bibitem[\protect\citeauthoryear{Houck \& Smith-Moore}{1988}]{hou88}
%  Houk, N., Smith-Moore, M., 1988, Michigan Spectral Survey, Ann
%  Arbor, Dept. of Astronomy, Univ. Michigan (Vol. 4)
%
%\bibitem[\protect\citeauthoryear{Iglesias-Groth}{2005}]{igl05} Iglesias-Groth, S., 2005, ApJ, 632, 25.
%
\bibitem[Kogut et al. (1996)]{kog96} Kogut, A., Banday, A. J., Bennett, C. L.,
  Gorski, K. M., Hinshaw, G. \& Reach, W. T.  1996,  ApJ, 460, 1

%
%\bibitem[\protect\citeauthoryear{Iglesias-Groth}{2006}]{igl06}
%  Iglesias-Groth, S., 2006, MNRAS, 368, 1925.
%
%\bibitem[\protect\citeauthoryear{Lanz \& Hubeny}{2007}]{lan07}
%  Lanz, T.,  Hubeny, I. 2007, ApJS, 169, 83.
%
\bibitem[\protect\citeauthoryear{Leitch et al.}{1997}]{lei97} Leitch,
  E.M., Readhead, A.C.S.,  Pearson,   T.J., Myers, S.T., 1997, ApJL 486, L23
%
%
%
%\bibitem[\protect\citeauthoryear{Lada \& Wilking}{1988}]{lad88} 
%Lada, C.J., Wilking, B.A., 1988, ApJ, 287, 610.
%
%
%\bibitem[\protect\citeauthoryear{Liseau et al.}{1995}]{lis95} Liseau,
%  R., Lorenzetti, D., Molinari, S., et al., 1995, A\&A, 300, 493. 
%
%\bibitem[\protect\citeauthoryear{Liseau et al.}{1999}]{lis99} Liseau,
%  R.,   et al., 1999, A\&A, 344, 342. 
%
%
%\bibitem[\protect\citeauthoryear{Lockman et al.}{1996}]{loc96}
%  Lockman, F.J., Pisano, D.J., Howard, G.J., 1996, ApJ, 472, 173
%
%\bibitem[\protect\citeauthoryear{Lynds}{1962}]{lyn62} Lynds, B.T.,
%  1962, ApJS, 7, 1
%
%\bibitem[\protect\citeauthoryear{Martin}{2007}]{mar07}
%Martin, P.G., 2007, EAS Publications Series, 23, 165.
%
%\bibitem[\protect\citeauthoryear{Mathis et al.}{1983}]{mat83}
%  Mathis, J.S., Mezger, P.G., Panagia, N., 1983, A\&A, 128, 212
%
%
%
%\bibitem[\protect\citeauthoryear{Miville-Deschenes \&
%    Lagache}{2005}]{miv05} Miville-Deschenes., M., Lagache, G., 2005,
%  ApJS, 157, 302.
%
%\bibitem[\protect\citeauthoryear{Nahar \& Pradhan}{1997}]{nah97}
%  Nahar, S.N., Pradhan, A.K., 1997, ApJSS, 111, 339.
%
%\bibitem[\protect\citeauthoryear{Padgett et al.}{2008}]{pad08}
%  Padgett, D. L., et al., 2008, ApJ, 672, 1013. 
%
% 
%
%
%\bibitem[\protect\citeauthoryear{Padin et al.}{2002}]{pad02} Padin, S., et al, 2002, PASP, 114, 83
%
%
%\bibitem[\protect\citeauthoryear{Paodan et al.}{2006}]{pao06} Padoan
%  P., Juvela M., Pelkonen V.-M., 2006, ApJ, 636, 101
%
\bibitem[\protect\citeauthoryear{Pankonin \& Walmsley}{1978}]{pan78}
  Pankonin, V., Walmsley, C.M., 1978, A\&A, 64, 333.
%
%\bibitem[\protect\citeauthoryear{Readhead et al.}{2004}]{rea04}  Readhead, A. C. S., et al., 2004, Science, 306, 836
%
%\bibitem[\protect\citeauthoryear{Reich et al.}{1990}]{rei90} Reich,
%  W., Fuerst, E., Reich, P., Reif, K., 1990, A\&AS, 85, 633
%
%\bibitem[\protect\citeauthoryear{Ridge et al.}{2006}]{rid06} Ridge,
%  N.A., et al., 2006, AJ, 131, 2921.
%
%\bibitem[\protect\citeauthoryear{Salem \& Brocklehurst}{1979}]{sal79}
%  Salem, M., Brocklehurst, M., 1979, ApJS, 39, 633. 


\bibitem[Scaife et al. (2009)]{sca09} Ami
  Consortium; Scaife, A. M. M., et al., 2009, MNRAS, 394, 46

%
%\bibitem[\protect\citeauthoryear{Schaller et al.}{1992}]{sch92}
%  Schaller, G., Schaerer, D., Meynet, G., Maeder, A., 1992, A\&AS, 96, 269
%
%
%\bibitem[Shepherd(1997)]{she97} Shepherd, M.C., 1997, in Astronomical
%  Data Analysis Software and Systems VI, ed. G~Hunt \& H.E.~Payne, ASP
%  conference series, v125, 77-84 ``Difmap: an interactive program for
%  synthesis imaging''.
%
%\bibitem[\protect\citeauthoryear{Schlegel et al.}{1998}]{sch98}
%  Schlegel, D.J., Finkbeiner, D. P., Davis, M., 1998, ApJ, 500, 525
%
%
%\bibitem[\protect\citeauthoryear{Smith et al.}{2004}]{smi04}
%  Smith, J. D. T., Dale, D. A., Armus, L., et al. 2004, ApJS,
%  154, 199
%
%%^\bibitem[\protect\citeauthoryear{Storey \& Hummer}{1995}]{sto95}
%%Storey, P. J., Hummer, D. G.,  1995, MNRAS, 272, 41
%
%
%\bibitem[\protect\citeauthoryear{Stutzki et al.}{1998}]{stu98}
%  Stutzki, J., Bensch, F., Heithausen, A., Ossenkopfm V., Zielinsky,
%  M., 1998, A\&A, 336, 697. 
%
%\bibitem[\protect\citeauthoryear{Tielens}{2005}]{tie05}
%  Tielens, A.G.G.M., 2005, ``The Physics and Chemistry of the
%  Interstellar Medium'', ISBN 0521826349. Cambridge, UK: Cambridge
%  University Press.
%
%

\bibitem[Vidal et al. (2010)]{vid10} Vidal, M., Casassus, S.,
  Dickinson, C., Witt, A., Castellanos, P., Paladini, R., Cleary, K.,
  Davis, R., Davies, R., Taylor, A., 2010, MNRAS, {\em in prep} 

\bibitem[Watson et al. (2005)]{wat05} Watson,
R. A., Rebolo, R., Rubi\~no-Mart\'in, J. A., Hildebrandt, S.,
Guti\'errez, C. M., Fern\'andez-Cerezo, S., Hoyland, R. J.,
Battistelli, E. S., 2005, ApJ, 624, 89


%\bibitem[\protect\citeauthoryear{Walmsley \& Watson}{1982}]{wal82}  Walmsley, C.M., Watson, W.D., 1982, ApJ, 260, 317. 
%
%\bibitem[\protect\citeauthoryear{Webster}{1992}]{web92}  Webster, A.,
%  1992, MNRAS, 255, 41
%
%\bibitem[\protect\citeauthoryear{Wheelock et al.}{1991}]{whe91}  Wheelock, et al., 1991, IRAS Sky Survey Atlas Explanatory Supplement..
%
%\bibitem[\protect\citeauthoryear{Witt et al.}{1998}]{wit98} Witt,
%  A.N., Gordon, K.D., Furton, D.G., 1998, ApJL, 501, 111 
%
%\bibitem[\protect\citeauthoryear{Wootten et al.}{1979}]{woo79}
%  Wootten, A., Snell, R., Glassgold, A. E., 1979, ApJ, 234, 876
%
%\bibitem[\protect\citeauthoryear{Wootten et al.}{1978}]{woo78}  Wootten, A.,
%  Evans, N. J., II, Snell, R., vanden Bout, P, 1978, ApJL, 225, 143
%
%
%\bibitem[\protect\citeauthoryear{Young et al.}{2006}]{you06} Young, K.E., et
%  al., 2006, ApJ, 644, 326 
%
%\bibitem[\protect\citeauthoryear{Yui et al.}{1993}]{yui93} Yui, Y.Y.,
%  Nakawaga, T., Doi, Y., Okuda, H., Shibai, H., Nishimura, T., Low,
%  F., 1993, ApJ, 419, L37.
%
%





	

\end{thebibliography}


\label{lastpage}

\end{document}
