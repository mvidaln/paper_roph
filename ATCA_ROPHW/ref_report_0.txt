https://mail.google.com/mail/u/0/#search/MNRAS/FMfcgxwKjTNXrtvVBCvGFZSSDPLFkjvZ


Dear Dr Casassus,

Copied below are the comments on your manuscript entitled "Resolved spectral variations of the centimetre-wavelength continuum from the Rho Oph W photo-dissociation-region", ref. MN-20-4020-MJ, which you submitted to Monthly Notices of the Royal Astronomical Society.

Minor revision of your manuscript is requested before it is reconsidered for publication.

You should submit your revised version, together with your response to any comments from the editor and reviewer at https://mc.manuscriptcentral.com/mnras. The deadline for this is six months from today.

Enter your Author Centre, where you will find your manuscript title listed under "Manuscripts with Decisions". Under "Actions," click on "Create a Revision". Please ensure that you also respond to any comments from the editor or assistant editor.

IMPORTANT: do not submit your revised manuscript as a new paper!

When submitting your revised manuscript, you should provide details of any changes you make to the original manuscript. Changes to the manuscript should also be highlighted (e.g. in bold or colour), to assist the referee and editor.

We look forward to receiving your revised manuscript.

Regards,

Helen

Dr Helen Klus
Assistant Editor MNRAS
Royal Astronomical Society

cc: all listed co-authors.


Reviewer's Comments:
Reviewer: Dr Yvette Chanel Perrott

Comments to the Author
This is an interesting and novel result describing a multi-frequency analysis of a region showing excess microwave emission, attributed to spinning dust.  The clear shift in peak intensity with frequency in this kind of emission has not been shown before and the result is definitely significant enough to warrant publication in MNRAS.

The paper is well organised and the analysis seems very robust.  I have minor comments only, as follows:

Introduction:

Paragraph 1:
- Needs some fleshing out with references for the detection of the anomalous foreground and its correlation with the far-IR
- “𝛼_radio/IR ∼ 0 in the range 15-30 GHz”   Not entirely clear what this means.  Is the spectral index between radio and far-IR wavelengths, or across the 15-30 GHz radio band?  If the latter, shouldn’t it be rising spectrum?  If the former, isn’t the IR much brighter than the radio?
- “The absence of H𝛼 emission…” —> “The observed absence of H𝛼 emission…”  (insert observed otherwise it sounds like a hypothetical statement rather than a consequence of observations)

Paragraph 2:
- AMI Consortium references are now requested to be cited as “First author” et al
- “A comprehensive review of all-sky surveys and targeted observations supports this spinning dust interpretation (e.g. Dickinson et al. 2018), which has been succesful in reproducing spectral variations from source to source in terms of local physical conditions.”  Is this somewhat of an overstatement?  My impression from Dickinson+ 2018 is that we are still relatively far from understanding which physical conditions produce AME, although SPDUST models are clearly able to produce a fit to spectra in different regions.  Also note “succesful” —> “successful”.

Paragraph 4:
- PDR already defined earlier

Paragraph 5:
- Define S1, might be helpful to add an IR map of the region with various features labelled, ie SR 3 referred to later on is also undefined.  Or add a reference to another paper with such a figure.
- “marginally discarded in the CBI data” Discarded is the wrong word here.  Do you mean “marginally disfavoured”?  Or “marginally ruled out by”?

Section 2.1: 
- Last sentence: “This gives an idea of the impact of spatial filtering by the interferometer.”  Sounds like you have only made some kind of rough comparison, rather than come up with a robust method for accounting for the uv-sampling as you have done.  Suggest to reword as something like “This correctly accounts for the spatial filtering by the interferometer and allows a robust comparison between wavebands.”

Suggested added section:  Since a lot of your analysis is based on the IRAC 8um data it would be helpful to have a section after the ATCA data section introducing the IRAC data where you give details such as resolution, display an unfiltered map, and describe point source filtering

Section 2.2:
- “Acrminute” —> “Arcminute”
- “The IRAC 8 𝜇m comparison images … have been filtered for the frequency-dependent 𝑢𝑣-coverage, and scaled in intensity so as to match the ATCA data”. Is this really correct?  If they have been scaled in intensity then wouldn’t the a value just be 1?  I’m assuming what you have really done is to treat the IRAC data in the same way as the ATCA images, as described in Appendix A - filter with the appropriate uv-coverage to create a dirty image, scale the unfiltered image to make a prior image, reconstruct using skymem, convolve with clean beam, then compare the resulting image with the ATCA image using equation 1 to get the slope

Table 1:
- “x” —> $\times$ in the beams
- add a horizontal line under the header

Fig 1 caption:
- After “𝑥− and 𝑦−axis show … in degrees.” add “Note that fields of view are different as appropriate for each frequency”
- “grey scale” —> “colour scale”
- “Identical restorations of IRAC 8 𝜇m visibilities, obtained by simulating ATCA+CABB observations” add “at each frequency” for clarity

Section 3.1, paragraph 2:
- “A multi-frequency comparison…” this makes it sounds like you’ve combined all the frequencies into the RGB map but if I’ve understood correctly it’s just 17 and 39 GHz. If so, then “multi-frequency” —> “dual-frequency”
- “17.481 GHz and 39.157 GHz” in other places just referred to as 17 and 39 GHz, check consistency in decimal places

Section 3.2:
-  Paragraph 1, first sentence: remove “may” - it *does* allow for estimates of the SED!
- Paragraph 1, last sentence: “espcially” —> “especially”
- Paragraph 2, footnote: it wasn’t initially clear what the LLS fit was to.  I guess it is to intensities in the 17 GHz peak region against intensities in the 39 GHz peak region.  This could be made clearer and moved into the main text to back up your statement that the difference is insignificant.

Section 3.2.1
- Paragraph 2: rogue bracket after definition of x_C
- Paragraph 3: Up until now you have not assumed PAHs as the carrier.  The SPDUST model should also work for other carriers according to Dickinson et al 2018, so could this part be edited to avoid the PAH assumption?  If not, some more detail should be added here or in the introduction about why a PAH carrier is being assumed.
- Add some more detail here about how the fitting was done.  Was it a maximum likelihood method?  Are there any degeneracies between the fitted parameters?
- You discuss the variation in a_cutoff between the fits, but beta also has different values at a similar significance of ~0.5 sigma.  Is beta degenerate with a_cutoff?  Does it make physical sense for beta to be different in the two regions?  If not, could you do a joint fit to both the regions forcing beta to be the same but allowing a_cutoff to be different, and would this improve the significance of the a_cutoff variation?
- 3rd-to-last paragraph: the comparison with the WISE ratio image is interesting and could be interpreted more.  How does this relate to the Hensley et al 2016 result?  Does this suggest that the lack of correlation between the PAH and AME abundances seen by those authors could be a resolution effect?
- 3rd-to-last paragraph: are you confident that the correlation analysis with WISE is not affected by the interferometric filtering?
- 2nd-to-last paragraph: is the offset of the 20 GHz point significant enough to be discussed?  It looks like ~1 sigma on the plot

Fig 2, caption:
- Presumably the blue and green lines have also been scaled to match the radio intensity at each frequency

Section 4:
- Paragraph 2: “signal cm-wavelength signal” - remove first “signal”
- Second-to-last paragraph: Any prospects for observations with another instrument with better frequency resolution?  ALMA?
- Last paragraph: “$Te$” —> “$T_{\mathrm{e}}$”.  What is the implication of the T_e<100K calculation for the observed signal?  “LT” —> “LTE”.  What is b?

Appendix A:
- This is a really interesting method which could be useful in other cases.  Would you consider making the code public? 
- It would be great to see a dirty map, prior image, model image, residual image and clean image for one or two of the frequencies to see how well the method works

General remarks:

- Please check consistency throughout in how you refer to the ATCA data.   It’s referred to variously as ATCA, ATCA+CABB, and CABB which may be confusing for someone not familiar with ATCA and its instrumentation.  I think it is all CABB data so just referring to it as ATCA would be fine.

- Tables: please put descriptions of table columns into caption rather than footnotes
