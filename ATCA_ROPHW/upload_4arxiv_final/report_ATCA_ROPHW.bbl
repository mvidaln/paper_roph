\begin{thebibliography}{}
\makeatletter
\relax
\def\mn@urlcharsother{\let\do\@makeother \do\$\do\&\do\#\do\^\do\_\do\%\do\~}
\def\mn@doi{\begingroup\mn@urlcharsother \@ifnextchar [ {\mn@doi@}
  {\mn@doi@[]}}
\def\mn@doi@[#1]#2{\def\@tempa{#1}\ifx\@tempa\@empty \href
  {http://dx.doi.org/#2} {doi:#2}\else \href {http://dx.doi.org/#2} {#1}\fi
  \endgroup}
\def\mn@eprint#1#2{\mn@eprint@#1:#2::\@nil}
\def\mn@eprint@arXiv#1{\href {http://arxiv.org/abs/#1} {{\tt arXiv:#1}}}
\def\mn@eprint@dblp#1{\href {http://dblp.uni-trier.de/rec/bibtex/#1.xml}
  {dblp:#1}}
\def\mn@eprint@#1:#2:#3:#4\@nil{\def\@tempa {#1}\def\@tempb {#2}\def\@tempc
  {#3}\ifx \@tempc \@empty \let \@tempc \@tempb \let \@tempb \@tempa \fi \ifx
  \@tempb \@empty \def\@tempb {arXiv}\fi \@ifundefined
  {mn@eprint@\@tempb}{\@tempb:\@tempc}{\expandafter \expandafter \csname
  mn@eprint@\@tempb\endcsname \expandafter{\@tempc}}}

\bibitem[\protect\citeauthoryear{{Ali-Ha{\"\i}moud}}{{Ali-Ha{\"\i}moud}}{2014}]{Ali-Haimoud2014}
{Ali-Ha{\"\i}moud} Y.,  2014, \mn@doi [\mnras] {10.1093/mnras/stt2083}, \href
  {https://ui.adsabs.harvard.edu/abs/2014MNRAS.437.2728A} {437, 2728}

\bibitem[\protect\citeauthoryear{Ali-Ha\"{\i}moud, Hirata  \&
  Dickinson}{Ali-Ha\"{\i}moud et~al.}{2009}]{Ali-Haimoud2009}
Ali-Ha\"{\i}moud Y.,  Hirata C.~M.,   Dickinson C.,  2009, \mn@doi [Monthly
  Notices of the Royal Astronomical Society]
  {10.1111/j.1365-2966.2009.14599.x}, 395, 1055

\bibitem[\protect\citeauthoryear{{Allamandola}, {Tielens}  \&
  {Barker}}{{Allamandola} et~al.}{1985}]{allamandola+85}
{Allamandola} L.~J.,  {Tielens} A.~G.~G.~M.,   {Barker} J.~R.,  1985, \mn@doi
  [\apjl] {10.1086/184435}, \href
  {https://ui.adsabs.harvard.edu/abs/1985ApJ...290L..25A} {290, L25}

\bibitem[\protect\citeauthoryear{{Arce-Tord} et~al.,}{{Arce-Tord}
  et~al.}{2020}]{Arce-Tord2020MNRAS.495.3482A}
{Arce-Tord} C.,  et~al., 2020, \mn@doi [\mnras] {10.1093/mnras/staa1422}, \href
  {https://ui.adsabs.harvard.edu/abs/2020MNRAS.495.3482A} {495, 3482}

\bibitem[\protect\citeauthoryear{{Brown} \& {Knapp}}{{Brown} \&
  {Knapp}}{1974}]{1974ApJ...189..253B}
{Brown} R.~L.,  {Knapp} G.~R.,  1974, \mn@doi [\apj] {10.1086/152796}, \href
  {http://adsabs.harvard.edu/abs/1974ApJ...189..253B} {189, 253}

\bibitem[\protect\citeauthoryear{{C{\'a}rcamo}, {Rom{\'a}n}, {Casassus},
  {Moral}  \& {Rannou}}{{C{\'a}rcamo}
  et~al.}{2018}]{Carcamo2018A&C....22...16C}
{C{\'a}rcamo} M.,  {Rom{\'a}n} P.~E.,  {Casassus} S.,  {Moral} V.,   {Rannou}
  F.~R.,  2018, \mn@doi [Astronomy and Computing]
  {10.1016/j.ascom.2017.11.003}, \href
  {http://adsabs.harvard.edu/abs/2018A%26C....22...16C} {22, 16}

\bibitem[\protect\citeauthoryear{{Casassus}, {Cabrera}, {F{\"o}rster},
  {Pearson}, {Readhead}  \& {Dickinson}}{{Casassus}
  et~al.}{2006}]{Casassus2006ApJ...639..951C}
{Casassus} S.,  {Cabrera} G.~F.,  {F{\"o}rster} F.,  {Pearson} T.~J.,
  {Readhead} A.~C.~S.,   {Dickinson} C.,  2006, \mn@doi [\apj]
  {10.1086/499517}, \href {http://adsabs.harvard.edu/abs/2006ApJ...639..951C}
  {639, 951}

\bibitem[\protect\citeauthoryear{{Casassus} et~al.,}{{Casassus}
  et~al.}{2008}]{cas08}
{Casassus} S.,  et~al., 2008, \mn@doi [\mnras]
  {10.1111/j.1365-2966.2008.13954.x}, \href
  {http://adsabs.harvard.edu/abs/2008MNRAS.391.1075C} {391, 1075}

\bibitem[\protect\citeauthoryear{{Casassus} et~al.,}{{Casassus}
  et~al.}{2018}]{Casassus2018MNRAS.477.5104C}
{Casassus} S.,  et~al., 2018, \mn@doi [\mnras] {10.1093/mnras/sty894}, \href
  {http://adsabs.harvard.edu/abs/2018MNRAS.477.5104C} {477, 5104}

\bibitem[\protect\citeauthoryear{{Casassus} et~al.,}{{Casassus}
  et~al.}{2019}]{Casassus2019MNRAS.483.3278C}
{Casassus} S.,  et~al., 2019, \mn@doi [\mnras] {10.1093/mnras/sty3269}, \href
  {http://adsabs.harvard.edu/abs/2019MNRAS.483.3278C} {483, 3278}

\bibitem[\protect\citeauthoryear{{Castellanos} et~al.,}{{Castellanos}
  et~al.}{2011}]{Castellanos2011MNRAS.411.1137C}
{Castellanos} P.,  et~al., 2011, \mn@doi [\mnras]
  {10.1111/j.1365-2966.2010.17743.x}, \href
  {https://ui.adsabs.harvard.edu/abs/2011MNRAS.411.1137C} {411, 1137}

\bibitem[\protect\citeauthoryear{{Cepeda-Arroita} et~al.,}{{Cepeda-Arroita}
  et~al.}{2020}]{Cepeda-Arroita2020arXiv200107159C}
{Cepeda-Arroita} R.,  et~al., 2020, arXiv e-prints, \href
  {https://ui.adsabs.harvard.edu/abs/2020arXiv200107159C} {p. arXiv:2001.07159}

\bibitem[\protect\citeauthoryear{{Cieza} et~al.,}{{Cieza}
  et~al.}{2019}]{Cieza2019MNRAS.482..698C}
{Cieza} L.~A.,  et~al., 2019, \mn@doi [\mnras] {10.1093/mnras/sty2653}, \href
  {https://ui.adsabs.harvard.edu/abs/2019MNRAS.482..698C} {482, 698}

\bibitem[\protect\citeauthoryear{{Croiset}, {Candian}, {Bern{\'e}}  \&
  {Tielens}}{{Croiset} et~al.}{2016}]{croiset+16}
{Croiset} B.~A.,  {Candian} A.,  {Bern{\'e}} O.,   {Tielens} A.~G.~G.~M.,
  2016, \mn@doi [\aap] {10.1051/0004-6361/201527714}, \href
  {https://ui.adsabs.harvard.edu/abs/2016A&A...590A..26C} {590, A26}

\bibitem[\protect\citeauthoryear{{Davies}, {Dickinson}, {Banday}, {Jaffe},
  {G{\'o}rski}  \& {Davis}}{{Davies} et~al.}{2006}]{dav06}
{Davies} R.~D.,  {Dickinson} C.,  {Banday} A.~J.,  {Jaffe} T.~R.,  {G{\'o}rski}
  K.~M.,   {Davis} R.~J.,  2006, \mn@doi [\mnras]
  {10.1111/j.1365-2966.2006.10572.x}, \href
  {http://adsabs.harvard.edu/abs/2006MNRAS.370.1125D} {370, 1125}

\bibitem[\protect\citeauthoryear{{Dickinson} et~al.,}{{Dickinson}
  et~al.}{2018}]{Dickinson2018NewAR..80....1D}
{Dickinson} C.,  et~al., 2018, \mn@doi [\nar] {10.1016/j.newar.2018.02.001},
  \href {https://ui.adsabs.harvard.edu/abs/2018NewAR..80....1D} {80, 1}

\bibitem[\protect\citeauthoryear{{Draine} \& {Hensley}}{{Draine} \&
  {Hensley}}{2012}]{DraineHensley2012ApJ...757..103D}
{Draine} B.~T.,  {Hensley} B.,  2012, \mn@doi [\apj]
  {10.1088/0004-637X/757/1/103}, \href
  {https://ui.adsabs.harvard.edu/abs/2012ApJ...757..103D} {757, 103}

\bibitem[\protect\citeauthoryear{{Draine} \& {Lazarian}}{{Draine} \&
  {Lazarian}}{1998a}]{dra98}
{Draine} B.~T.,  {Lazarian} A.,  1998a, \mn@doi [\apjl] {10.1086/311167}, \href
  {http://adsabs.harvard.edu/abs/1998ApJ...494L..19D} {494, L19+}

\bibitem[\protect\citeauthoryear{{Draine} \& {Lazarian}}{{Draine} \&
  {Lazarian}}{1998b}]{dl98b}
{Draine} B.~T.,  {Lazarian} A.,  1998b, \mn@doi [\apj] {10.1086/306387}, \href
  {http://adsabs.harvard.edu/abs/1998ApJ...508..157D} {508, 157}

\bibitem[\protect\citeauthoryear{{Draine} \& {Lazarian}}{{Draine} \&
  {Lazarian}}{1999}]{DraineLazarian1999ApJ...512..740D}
{Draine} B.~T.,  {Lazarian} A.,  1999, \mn@doi [\apj] {10.1086/306809}, \href
  {https://ui.adsabs.harvard.edu/abs/1999ApJ...512..740D} {512, 740}

\bibitem[\protect\citeauthoryear{{Elias}}{{Elias}}{1978}]{Elias1978ApJ...224..453E}
{Elias} J.~H.,  1978, \mn@doi [\apj] {10.1086/156393}, \href
  {https://ui.adsabs.harvard.edu/abs/1978ApJ...224..453E} {224, 453}

\bibitem[\protect\citeauthoryear{{Finkbeiner}, {Schlegel}, {Frank}  \&
  {Heiles}}{{Finkbeiner} et~al.}{2002}]{Finkbeiner2002ApJ...566..898F}
{Finkbeiner} D.~P.,  {Schlegel} D.~J.,  {Frank} C.,   {Heiles} C.,  2002,
  \mn@doi [\apj] {10.1086/338225}, \href
  {http://adsabs.harvard.edu/abs/2002ApJ...566..898F} {566, 898}

\bibitem[\protect\citeauthoryear{{Gaia Collaboration}}{{Gaia
  Collaboration}}{2018}]{Gaia2018yCat.1345....0G}
{Gaia Collaboration} 2018, VizieR Online Data Catalog, \href
  {https://ui.adsabs.harvard.edu/abs/2018yCat.1345....0G} {p. I/345}

\bibitem[\protect\citeauthoryear{{Garufi} et~al.,}{{Garufi}
  et~al.}{2020}]{Garufi2020A&A...633A..82G}
{Garufi} A.,  et~al., 2020, \mn@doi [\aap] {10.1051/0004-6361/201936946}, \href
  {https://ui.adsabs.harvard.edu/abs/2020A&A...633A..82G} {633, A82}

\bibitem[\protect\citeauthoryear{{Gold} et~al.,}{{Gold}
  et~al.}{2011}]{Gold2011ApJS..192...15G}
{Gold} B.,  et~al., 2011, \mn@doi [\apjs] {10.1088/0067-0049/192/2/15}, \href
  {https://ui.adsabs.harvard.edu/abs/2011ApJS..192...15G} {192, 15}

\bibitem[\protect\citeauthoryear{{Habart}, {Boulanger}, {Verstraete}, {Pineau
  des For{\^e}ts}, {Falgarone}  \& {Abergel}}{{Habart}
  et~al.}{2003}]{Habart2003}
{Habart} E.,  {Boulanger} F.,  {Verstraete} L.,  {Pineau des For{\^e}ts} G.,
  {Falgarone} E.,   {Abergel} A.,  2003, \mn@doi [\aap]
  {10.1051/0004-6361:20021489}, \href
  {http://adsabs.harvard.edu/abs/2003A%26A...397..623H} {397, 623}

\bibitem[\protect\citeauthoryear{{Hensley} \& {Draine}}{{Hensley} \&
  {Draine}}{2017}]{Hensley2017ApJ...836..179H}
{Hensley} B.~S.,  {Draine} B.~T.,  2017, \mn@doi [\apj]
  {10.3847/1538-4357/aa5c37}, \href
  {https://ui.adsabs.harvard.edu/abs/2017ApJ...836..179H} {836, 179}

\bibitem[\protect\citeauthoryear{{Hensley}, {Draine}  \& {Meisner}}{{Hensley}
  et~al.}{2016}]{Hensley2016}
{Hensley} B.~S.,  {Draine} B.~T.,   {Meisner} A.~M.,  2016, \mn@doi [\apj]
  {10.3847/0004-637X/827/1/45}, \href
  {https://ui.adsabs.harvard.edu/abs/2016ApJ...827...45H} {827, 45}

\bibitem[\protect\citeauthoryear{{Hoang} \& {Lazarian}}{{Hoang} \&
  {Lazarian}}{2016}]{HoangLazarian2016ApJ...821...91H}
{Hoang} T.,  {Lazarian} A.,  2016, \mn@doi [\apj] {10.3847/0004-637X/821/2/91},
  \href {https://ui.adsabs.harvard.edu/abs/2016ApJ...821...91H} {821, 91}

\bibitem[\protect\citeauthoryear{{Hoang}, {Draine}  \& {Lazarian}}{{Hoang}
  et~al.}{2010}]{Hoang2010ApJ...715.1462H}
{Hoang} T.,  {Draine} B.~T.,   {Lazarian} A.,  2010, \mn@doi [\apj]
  {10.1088/0004-637X/715/2/1462}, \href
  {http://adsabs.harvard.edu/abs/2010ApJ...715.1462H} {715, 1462}

\bibitem[\protect\citeauthoryear{{Hoang}, {Vinh}  \& {Quynh Lan}}{{Hoang}
  et~al.}{2016}]{Hoang2016ApJ...824...18H}
{Hoang} T.,  {Vinh} N.-A.,   {Quynh Lan} N.,  2016, \mn@doi [\apj]
  {10.3847/0004-637X/824/1/18}, \href
  {https://ui.adsabs.harvard.edu/abs/2016ApJ...824...18H} {824, 18}

\bibitem[\protect\citeauthoryear{{H{\"o}gbom}}{{H{\"o}gbom}}{1974}]{Hogbom1974A&AS...15..417H}
{H{\"o}gbom} J.~A.,  1974, \aaps, \href
  {https://ui.adsabs.harvard.edu/abs/1974A&AS...15..417H} {15, 417}

\bibitem[\protect\citeauthoryear{{Kogut}, {Banday}, {Bennett}, {Gorski},
  {Hinshaw}  \& {Reach}}{{Kogut} et~al.}{1996}]{kog96}
{Kogut} A.,  {Banday} A.~J.,  {Bennett} C.~L.,  {Gorski} K.~M.,  {Hinshaw} G.,
   {Reach} W.~T.,  1996, \mn@doi [\apj] {10.1086/176947}, \href
  {http://adsabs.harvard.edu/abs/1996ApJ...460....1K} {460, 1}

\bibitem[\protect\citeauthoryear{{Lada} \& {Wilking}}{{Lada} \&
  {Wilking}}{1984}]{LadaWilking1984ApJ...287..610L}
{Lada} C.~J.,  {Wilking} B.~A.,  1984, \mn@doi [\apj] {10.1086/162719}, \href
  {https://ui.adsabs.harvard.edu/abs/1984ApJ...287..610L} {287, 610}

\bibitem[\protect\citeauthoryear{{Le Petit}, {Nehm{\'e}}, {Le Bourlot}  \&
  {Roueff}}{{Le Petit} et~al.}{2006}]{LePetit2006ApJS..164..506L}
{Le Petit} F.,  {Nehm{\'e}} C.,  {Le Bourlot} J.,   {Roueff} E.,  2006, \mn@doi
  [\apjs] {10.1086/503252}, \href
  {http://adsabs.harvard.edu/abs/2006ApJS..164..506L} {164, 506}

\bibitem[\protect\citeauthoryear{{Leitch}, {Readhead}, {Pearson}  \&
  {Myers}}{{Leitch} et~al.}{1997}]{lei97}
{Leitch} E.~M.,  {Readhead} A.~C.~S.,  {Pearson} T.~J.,   {Myers} S.~T.,  1997,
  \mn@doi [\apjl] {10.1086/310823}, \href
  {http://adsabs.harvard.edu/abs/1997ApJ...486L..23L} {486, L23+}

\bibitem[\protect\citeauthoryear{{Liseau} et~al.,}{{Liseau}
  et~al.}{1999}]{1999A&A...344..342L}
{Liseau} R.,  et~al., 1999, \aap, \href
  {http://adsabs.harvard.edu/abs/1999A%26A...344..342L} {344, 342}

\bibitem[\protect\citeauthoryear{{Markwardt}}{{Markwardt}}{2009}]{MPFIT_2009}
{Markwardt} C.~B.,  2009, in {Bohlender} D.~A.,  {Durand} D.,   {Dowler} P.,
  eds,  Astronomical Society of the Pacific Conference Series Vol. 411,
  Astronomical Data Analysis Software and Systems XVIII. p.~251 (\mn@eprint
  {arXiv} {0902.2850})

\bibitem[\protect\citeauthoryear{{Pankonin} \& {Walmsley}}{{Pankonin} \&
  {Walmsley}}{1978}]{pan78}
{Pankonin} V.,  {Walmsley} C.~M.,  1978, \aap, \href
  {http://adsabs.harvard.edu/abs/1978A%26A....64..333P} {64, 333}

\bibitem[\protect\citeauthoryear{{Pattle} et~al.,}{{Pattle}
  et~al.}{2015}]{Pattle2015MNRAS.450.1094P}
{Pattle} K.,  et~al., 2015, \mn@doi [\mnras] {10.1093/mnras/stv376}, \href
  {https://ui.adsabs.harvard.edu/abs/2015MNRAS.450.1094P} {450, 1094}

\bibitem[\protect\citeauthoryear{{P{\'e}rez}, {Casassus}, {Baruteau}, {Dong},
  {Hales}  \& {Cieza}}{{P{\'e}rez} et~al.}{2019}]{Perez2019AJ....158...15P}
{P{\'e}rez} S.,  {Casassus} S.,  {Baruteau} C.,  {Dong} R.,  {Hales} A.,
  {Cieza} L.,  2019, \mn@doi [\aj] {10.3847/1538-3881/ab1f88}, \href
  {https://ui.adsabs.harvard.edu/abs/2019AJ....158...15P} {158, 15}

\bibitem[\protect\citeauthoryear{{Planck Collaboration} et~al.,}{{Planck
  Collaboration} et~al.}{2011}]{Planck2011A&A...536A..20P}
{Planck Collaboration} et~al., 2011, \mn@doi [\aap]
  {10.1051/0004-6361/201116470}, \href
  {https://ui.adsabs.harvard.edu/abs/2011A&A...536A..20P} {536, A20}

\bibitem[\protect\citeauthoryear{{Planck Collaboration} et~al.,}{{Planck
  Collaboration} et~al.}{2013}]{Planck2013A&A...557A..53P}
{Planck Collaboration} et~al., 2013, \mn@doi [\aap]
  {10.1051/0004-6361/201321160}, \href
  {https://ui.adsabs.harvard.edu/abs/2013A&A...557A..53P} {557, A53}

\bibitem[\protect\citeauthoryear{{Planck Collaboration} et~al.,}{{Planck
  Collaboration} et~al.}{2016a}]{Planck2016A&A...594A...1P}
{Planck Collaboration} et~al., 2016a, \mn@doi [\aap]
  {10.1051/0004-6361/201527101}, \href
  {https://ui.adsabs.harvard.edu/abs/2016A&A...594A...1P} {594, A1}

\bibitem[\protect\citeauthoryear{{Planck Collaboration} et~al.,}{{Planck
  Collaboration} et~al.}{2016b}]{Planck2016A&A...594A..10P}
{Planck Collaboration} et~al., 2016b, \mn@doi [\aap]
  {10.1051/0004-6361/201525967}, \href
  {https://ui.adsabs.harvard.edu/abs/2016A&A...594A..10P} {594, A10}

\bibitem[\protect\citeauthoryear{{Ricca}, {Bauschlicher}, {Boersma}, {Tielens}
  \& {Allamandola}}{{Ricca} et~al.}{2012}]{ricca+12}
{Ricca} A.,  {Bauschlicher} Charles~W. J.,  {Boersma} C.,  {Tielens} A.
  G.~G.~M.,   {Allamandola} L.~J.,  2012, \mn@doi [\apj]
  {10.1088/0004-637X/754/1/75}, \href
  {https://ui.adsabs.harvard.edu/abs/2012ApJ...754...75R} {754, 75}

\bibitem[\protect\citeauthoryear{{Ridge} et~al.,}{{Ridge} et~al.}{2006}]{rid06}
{Ridge} N.~A.,  et~al., 2006, \mn@doi [\aj] {10.1086/503704}, \href
  {http://adsabs.harvard.edu/abs/2006AJ....131.2921R} {131, 2921}

\bibitem[\protect\citeauthoryear{{Sault}, {Teuben}  \& {Wright}}{{Sault}
  et~al.}{1995}]{Miriad1995ASPC...77..433S}
{Sault} R.~J.,  {Teuben} P.~J.,   {Wright} M.~C.~H.,  1995, in {Shaw} R.~A.,
  {Payne} H.~E.,   {Hayes} J.~J.~E.,  eds,  Astronomical Society of the Pacific
  Conference Series Vol. 77, Astronomical Data Analysis Software and Systems
  IV. p.~433 (\mn@eprint {arXiv} {astro-ph/0612759})

\bibitem[\protect\citeauthoryear{{Scaife} et~al.,}{{Scaife}
  et~al.}{2009}]{Scaife2009MNRAS.394L..46A}
{Scaife} A.~M.~M.,  et~al., 2009, \mn@doi [\mnras]
  {10.1111/j.1745-3933.2008.00607.x}, \href
  {http://adsabs.harvard.edu/abs/2009MNRAS.394L..46A} {394, L46}

\bibitem[\protect\citeauthoryear{{Scaife} et~al.,}{{Scaife}
  et~al.}{2010}]{Scaife2010}
{Scaife} A. M.~M.,  et~al., 2010, \mn@doi [\mnras]
  {10.1111/j.1745-3933.2010.00812.x}, \href
  {https://ui.adsabs.harvard.edu/abs/2010MNRAS.403L..46S} {403, L46}

\bibitem[\protect\citeauthoryear{{Silsbee}, {Ali-Ha{\"i}moud}  \&
  {Hirata}}{{Silsbee} et~al.}{2011}]{Silsbee2011MNRAS.411.2750S}
{Silsbee} K.,  {Ali-Ha{\"i}moud} Y.,   {Hirata} C.~M.,  2011, \mn@doi [\mnras]
  {10.1111/j.1365-2966.2010.17882.x}, \href
  {http://adsabs.harvard.edu/abs/2011MNRAS.411.2750S} {411, 2750}

\bibitem[\protect\citeauthoryear{{Tibbs} et~al.,}{{Tibbs}
  et~al.}{2012}]{Tibss2012ApJ...754...94T}
{Tibbs} C.~T.,  et~al., 2012, \mn@doi [\apj] {10.1088/0004-637X/754/2/94},
  \href {https://ui.adsabs.harvard.edu/abs/2012ApJ...754...94T} {754, 94}

\bibitem[\protect\citeauthoryear{{Vidal} et~al.,}{{Vidal}
  et~al.}{2011}]{Vidal2011MNRAS.414.2424V}
{Vidal} M.,  et~al., 2011, \mn@doi [\mnras] {10.1111/j.1365-2966.2011.18562.x},
  \href {http://adsabs.harvard.edu/abs/2011MNRAS.414.2424V} {414, 2424}

\bibitem[\protect\citeauthoryear{{Vidal}, {Dickinson}, {Harper}, {Casassus}  \&
  {Witt}}{{Vidal} et~al.}{2020}]{Vidal2020MNRAS.495.1122V}
{Vidal} M.,  {Dickinson} C.,  {Harper} S.~E.,  {Casassus} S.,   {Witt} A.~N.,
  2020, \mn@doi [\mnras] {10.1093/mnras/staa1186}, \href
  {https://ui.adsabs.harvard.edu/abs/2020MNRAS.495.1122V} {495, 1122}

\bibitem[\protect\citeauthoryear{{Watson}, {Rebolo},
  {Rubi{\~n}o-Mart{\'{\i}}n}, {Hildebrandt}, {Guti{\'e}rrez},
  {Fern{\'a}ndez-Cerezo}, {Hoyland}  \& {Battistelli}}{{Watson}
  et~al.}{2005}]{Watson2005ApJ...624L..89W}
{Watson} R.~A.,  {Rebolo} R.,  {Rubi{\~n}o-Mart{\'{\i}}n} J.~A.,  {Hildebrandt}
  S.,  {Guti{\'e}rrez} C.~M.,  {Fern{\'a}ndez-Cerezo} S.,  {Hoyland} R.~J.,
  {Battistelli} E.~S.,  2005, \mn@doi [\apjl] {10.1086/430519}, \href
  {http://adsabs.harvard.edu/abs/2005ApJ...624L..89W} {624, L89}

\bibitem[\protect\citeauthoryear{{Weingartner} \& {Draine}}{{Weingartner} \&
  {Draine}}{2001}]{Weingartner2001}
{Weingartner} J.~C.,  {Draine} B.~T.,  2001, \mn@doi [\apj] {10.1086/318651},
  \href {http://ukads.nottingham.ac.uk/abs/2001ApJ...548..296W} {548, 296}

\bibitem[\protect\citeauthoryear{{White} et~al.,}{{White}
  et~al.}{2015}]{White2015MNRAS.447.1996W}
{White} G.~J.,  et~al., 2015, \mn@doi [\mnras] {10.1093/mnras/stu2323}, \href
  {https://ui.adsabs.harvard.edu/abs/2015MNRAS.447.1996W} {447, 1996}

\bibitem[\protect\citeauthoryear{{Wilson} et~al.,}{{Wilson}
  et~al.}{2011}]{CABB2011MNRAS.416..832W}
{Wilson} W.~E.,  et~al., 2011, \mn@doi [\mnras]
  {10.1111/j.1365-2966.2011.19054.x}, \href
  {https://ui.adsabs.harvard.edu/abs/2011MNRAS.416..832W} {416, 832}

\bibitem[\protect\citeauthoryear{{Ysard} \& {Verstraete}}{{Ysard} \&
  {Verstraete}}{2010}]{Ysard2010A&A...509A..12Y}
{Ysard} N.,  {Verstraete} L.,  2010, \mn@doi [\aap]
  {10.1051/0004-6361/200912708}, \href
  {http://adsabs.harvard.edu/abs/2010A%26A...509A..12Y} {509, A12}

\makeatother
\end{thebibliography}
