Dear editor and referee,

we thank the referee for her positive appreciation and constructive
comments. We have edited our manuscript accordingly, with particular
attention to the following points:


> - You discuss the variation in a_cutoff between the fits, but beta
    also has different values at a similar significance of ~0.5 sigma.
    Is beta degenerate with a_cutoff?  Does it make physical sense for
    beta to be different in the two regions?  If not, could you do a
    joint fit to both the regions forcing beta to be the same but
    allowing a_cutoff to be different, and would this improve the
    significance of the a_cutoff variation?

This is indeed a good idea. We think that the main result of our SED
fits is the need for a cutoff in minimum grain size, a_cutoff, without
which the spectrum would shift a much higher frequencies. Thus, it is
interesting to see that a tiny variation in a_cutoff alone can account
for the spectral trends. These trends might not be significant when
considering only the SED, on which we impose conservative 10%
errors. But they are quite evident in the morphology of the
nebula. This single parameter a_cutoff can, on its own, account for
these morphological differences.


> - It would be great to see a dirty map, prior image, model image,
  residual image and clean image for one or two of the frequencies to
  see how well the method works

We added information on skymem and included a panel with the requested
information. In doing so, by looking again at the residuals we
realised that the thermal errors were much smaller than reported
initially in our SED. The original errors were extracted from the
signal rms in the restored images directly. But since the signal is
extended, this noise was measured at the edge of the field and hence
exaggerated. The thermal uncertainties are actually impressively small
- and come close to the theoretical sensitivity of ATCA (we added a
table to this effect). But this does not qualitatively change our
analysis because of the 10% systematics.

In what follows we reply in more detail to the referee's comments,
which we have all implemented except for the editions to the format
for tables, in which we provide details as footnotes to the tables rather
than in the caption. In this respect we would like confirmation from the
editor, as we were previously instructed to format tables in this way.

We also include a list of additional changes at the end of this reply
to the referee. Among these we highlight a new subsection on the point
sources in the field (new Sec. 2.2), the addition of a table with the
rms dispersion of residuals, and the propagation of these new numbers
to the SED table.

We apologise for the numerous spelling mistakes and typos in our first
submission.

Best regards,
the authors.

----------------------------------------------------------------------


> Introduction:

> Paragraph 1:

> - Needs some fleshing out with references for the detection of the
    anomalous foreground and its correlation with the far-IR

Done, we added references to the WMAP and Planck missions and pointed
to the review by Dickinson et al 2018.


> - “𝛼_radio/IR ∼ 0 in the range 15-30 GHz” Not entirely clear what
    this means.  Is the spectral index between radio and far-IR
    wavelengths, or across the 15-30 GHz radio band?  If the latter,
    shouldn’t it be rising spectrum?  If the former, isn’t the IR much
    brighter than the radio?

We mean the former, this is the spectral index of the Galactic
foreground itself. We have reworded accordingly.

> - “The absence of H𝛼 emission…” —> “The observed absence of H𝛼
    emission…”  (insert observed otherwise it sounds like a
    hypothetical statement rather than a consequence of observations)

Done, thanks.


> Paragraph 2:

> - AMI Consortium references are now requested to be cited as “First
    author” et al

Done.


> - “A comprehensive review of all-sky surveys and targeted
    observations supports this spinning dust interpretation
    (e.g. Dickinson et al. 2018), which has been successful in
    reproducing spectral variations from source to source in terms of
    local physical conditions.”  Is this somewhat of an overstatement?
    My impression from Dickinson+ 2018 is that we are still relatively
    far from understanding which physical conditions produce AME,
    although SPDUST models are clearly able to produce a fit to
    spectra in different regions.  Also note “succesful” —>
    “successful”.

Thanks, we agree and have reworded this Paragraph: ".... A common
feature of all cm-bright clouds is that they host conspicuous photo-
dissociation-regions (PDRs). The Planck mission has also picked
up spectral variations in this excess microwave emission (EME)
from source to source along the Gould belt where the peak fre-
quency is ν peak ∼26–30 GHz while ν peak ∼ 25 GHz in the diffuse
ISM (Planck Collaboration et al. 2013, 2016b). The prevailing in-
terpretation for EME, also called anomalous microwave emission
(AME), is electric-dipole radiation from spinning very small grains,
or ‘spinning dust’ (Draine & Lazarian 1998a). A comprehensive
review of all-sky surveys and targeted observations supports this
spinning dust interpretation (Dickinson et al. 2018). The carriers
of spinning dust remain to be identified, however, and could be
PAHs (e.g. Ali-Haïmoud 2014), nano-silicates (Hoang et al. 2016;
Hensley & Draine 2017), or spinning magnetic dipoles (Hoang &
Lazarian 2016; Hensley & Draine 2017). A contribution to EME
from the thermal emission of magnetic dust (e.g. Draine & Lazarian
1999) may be important in some regions (Draine & Hensley 2012)."
 


> Paragraph 4:
> - PDR already defined earlier

thanks.

> Paragraph 5:

> - Define S1, might be helpful to add an IR map of the region with
    various features labelled, ie SR 3 referred to later on is also
    undefined.  Or add a reference to another paper with such a
    figure.

We added this material to the preceding paragraph:

" While the HD 147889 binary has the earliest spectral types in the
complex (B2iv and B3iv Casassus et al. 2008), the region also hosts
two other early-type stars: S 1 (which is a close binary including a
B4v star, Lada & Wilking 1984), and SR 3 (with spectral type B6v,
Elias 1978). Both S 1 and SR 3 are embedded in the molecular cloud. An
image of the region including the relative positions of these 3
early-type stars can be found in Casassus et al.  (2008, their Fig. 4)
or in Arce-Tord et al. (2020, their Fig. 2)."

Since both Casassus et al.  (2008) and Arce-Tord et al. (2020) include
the requested Figure, we thought it would be sufficient to point at
them.


>- “marginally discarded in the CBI data” Discarded is the wrong word
   here.  Do you mean “marginally disfavoured”?  Or “marginally ruled
   out by”?

We meant "ruled out", and have  split this Para in two and reworded the end  to:

" (...) This correlation is marginally ruled out in the CBI data,
which thus point at emissivity variations within the source (Casassus
et al. 2008). However, the CBI 2 observations, with a finer beam,
reveal that j(1cm)/nH varies by a factor of at least 26 at 3sigma. "



> Section 2.1: 

> - Last sentence: “This gives an idea of the impact of spatial
    filtering by the interferometer.”  Sounds like you have only made
    some kind of rough comparison, rather than come up with a robust
    method for accounting for the uv-sampling as you have done.
    Suggest to reword as something like “This correctly accounts for
    the spatial filtering by the interferometer and allows a robust
    comparison between wavebands.”

Thank you. We believe that the correction is good, but not perfect, as
it depends on the quality of the prior, which varies for different
bands. For instance, using IRAC8um as a prior is OK at 20GHz, but less
so at 39GHz. Since the use of the same prior promotes similar images,
we believe that this approach is conservative in picking up
morphological differences. In brief, we have adapted the referee's
wording to:

"This accounts for the spatial filtering by the interferometer and
allows a robust comparison between wavebands."

And have added the following material at the end of the previous paragraph:


"Probably the most important feature of skymem, which allowed the
recovery of the missing spatial frequencies, is the use of an image
template as initial condition. For adequate results this template must
correlate very tightly with the signal, and in this application we
used data from the Infrared Array Camera (IRAC) aboard Spitzer. The
c2d Spitzer Legacy Survey provided an 8 μm mosaic of the entire ρ Oph
region at an angular resolution of 2 arcsecond. Since the
cm-wavelength radio signal in the ρ Oph W filament is known to
correlate with IRAC 8 μm (Casassus et al. 2008; Arce-Tord et al.
2020). Since its resolution is much finer than that of the ATCA signal
we aim to image, and as it is relatively less crowded by point sources
compared to the shorter wavelengths, we adopted the IRAC 8 μm mosaic
as the skymem template after point-source subtraction by median
filtering. This template is the same as that used in (Casassus et
al. 2008), and is also shown here in the Appendix."



> Suggested added section: Since a lot of your analysis is based on
  the IRAC 8um data it would be helpful to have a section after the
  ATCA data section introducing the IRAC data where you give details
  such as resolution, display an unfiltered map, and describe point
  source filtering

More details on the IRAC8um mosaic are now given in the summary
description of skymem in the above paragraph, and the map itself is
shown in Appendix.


> Section 2.2:

> - “Acrminute” —> “Arcminute”

Thanks.

> - “The IRAC 8 𝜇m comparison images … have been filtered for the
  frequency-dependent 𝑢𝑣-coverage, and scaled in intensity so as to
  match the ATCA data”. Is this really correct?  If they have been
  scaled in intensity then wouldn’t the a value just be 1?  I’m
  assuming what you have really done is to treat the IRAC data in the
  same way as the ATCA images, as described in Appendix A - filter
  with the appropriate uv-coverage to create a dirty image, scale the
  unfiltered image to make a prior image, reconstruct using skymem,
  convolve with clean beam, then compare the resulting image with the
  ATCA image using equation 1 to get the slope


This formulation is indeed absurd! We thank the referee for picking
this up. We have reworded and expanded as follows:

"The IRAC 8 μm comparison images I8um(x_i,nu_i) , have been filtered
for the frequency-dependent uv-coverage and scaled in intensity to
approximate the range of intensities observed in EME sources (as
described in Appendix A). Specifically, we scale the IRAC 8 μm mosaic
by the slope of the CBI/IRAC 8 μm correlation measured in M 78 by
Castellanos et al. (2011).  For instance, the ratio of cm-
wavelength specific intensities relative to IRAC 8 μm are typically
3.05 times higher in ρ Oph W at 20 GHz than in M 78 at 31 GHz.
The slopes a(ν) are therefore dimensionless, and can be used as an
SED indicator, albeit in in arbitrary units."


> Table 1:
   - “x” —> $\times$ in the beams
   - add a horizontal line under the header

Thanks.


> Fig 1 caption:
> - After “𝑥− and 𝑦−axis show … in degrees.” add “Note that fields of
    view are different as appropriate for each frequency”
> - “grey scale” —> “colour scale”
> - “Identical restorations of IRAC 8 𝜇m visibilities, obtained by
    simulating ATCA+CABB observations” add “at each frequency” for
    clarity

Thanks.


> Section 3.1, paragraph 2:
>- “A multi-frequency comparison…” this makes it sounds like you’ve
   combined all the frequencies into the RGB map but if I’ve
   understood correctly it’s just 17 and 39 GHz. If so, then
   “multi-frequency” —> “dual-frequency”

Thanks. We have also replaced RGB by RG.

> - “17.481 GHz and 39.157 GHz” in other places just referred to as 17
    and 39 GHz, check consistency in decimal places

Thanks, we have removed the decimals for consistency. 

> Section 3.2:
> - Paragraph 1, first sentence: remove “may” - it *does* allow for
     estimates of the SED!

Thanks.

> - Paragraph 1, last sentence: “espcially” —> “especially”

Thanks.

> - Paragraph 2, footnote: it wasn’t initially clear what the LLS fit
    was to.  I guess it is to intensities in the 17 GHz peak region
    against intensities in the 39 GHz peak region.  This could be made
    clearer and moved into the main text to back up your statement
    that the difference is insignificant.

We moved the footnote back into the text and expanded it this way:

" A linear least squares for a model such as Iν (17 GHz mask) = αIν
(39 GHz mask), with no intercept but with errors in both coordinates,
yields a slope α = 1.07 ± 0.10. "


> Section 3.2.1

> - Paragraph 2: rogue bracket after definition of x_C

Thanks.

> - Paragraph 3: Up until now you have not assumed PAHs as the
    carrier.  The SPDUST model should also work for other carriers
    according to Dickinson et al 2018, so could this part be edited to
    avoid the PAH assumption?  If not, some more detail should be
    added here or in the introduction about why a PAH carrier is being
    assumed.

We have included a sentence saying that we are assuming that the
emission is originated by PAHs, justifying this by the excellent
correlation that the radio emission has with the 8um emission:

"The motivation for this assumption is the excellent
correlation between the radio emission and the 8 μm map in this
region (Casassus et al. 2008; Arce-Tord et al. 2020)."



> - Add some more detail here about how the fitting was done.  Was it a
  maximum likelihood method?  Are there any degeneracies between the
  fitted parameters?

We specified the fitting method (see next comment) and also include
some comments regarding the degeneracies of SPDUST. In principle the
physical parameters of the emitting medium are strongly correlated but
in this case we can avoid the problem due to the fact that we are
fixing most of the parameters from previous works in the literature.





> - You discuss the variation in a_cutoff between the fits, but beta
    also has different values at a similar significance of ~0.5 sigma.
    Is beta degenerate with a_cutoff?  Does it make physical sense for
    beta to be different in the two regions?  If not, could you do a
    joint fit to both the regions forcing beta to be the same but
    allowing a_cutoff to be different, and would this improve the
    significance of the a_cutoff variation?

This is an important point and we have implemented the referee's
suggestion. We went a step further by fixing also n_h, as keeping it
as a free parameter resulted in essentially the same values for both
masks.  Nevertheless, the difference in a_cutoff between both regions
is 1.8sigma (when using the new photometric extraction), which is not
so surprising since the two SEDs are not significantly different based
on the Chi2 test. 

Still, our main result with these SED fits is that a spinning-dust
interpretation requires a grain size cutoff, a_cutoff, and that this
single parameter can, on its own, account for the spectral trends in
rho Oph. These spectral trends correspond to significant morphological
variations, which is why we explore them even if the SEDs of their own
are not statistically significant when taking 10% errors.

We added this material to the article:

"The data in the SEDs were fitted using the IDL routine mpfit-
fun (Markwardt 2009), that uses the Levenberg-Marquardt least-
squares fit to a function. We performed the fit using the SPDUST
model for the two regions shown in Fig. 3. The result from this
initial fit gave very similar values between the two regions for n H
(3.0 ± 1.5 vs 3.1 ± 2.7) and β (38.7 ± 5.9 vs 35.1 ± 8.4). We thus
decided to fix n H and β and only fit for a cutoff ."



- 3rd-to-last paragraph: the comparison with the WISE ratio image is
  interesting and could be interpreted more.  How does this relate to
  the Hensley et al 2016 result?  Does this suggest that the lack of
  correlation between the PAH and AME abundances seen by those authors
  could be a resolution effect?

We suggested the resolution effect in Arce-Tord 2020. Here we have
separated a new paragraph to explain this apparent inconsistency:

"The AME-PAH connection was put in doubt by Hensley et al.(2016) based
on a full-sky analysis on angular scales of 1 deg.  Indeed, when taken
as a whole, the ρ Oph cloud is a good example of the breakdown of the
correlation between PAHs tracers and AME, since S 1, the brightest
nebula in the complex in IRAC 8μm and also in Spitzer-IRS 11.3μm PAH
band (Casassus et al. 2008, their Table 2), has no detectable EME
signal. However, it appears that EME and PAHs do correlate very
tighlty in higher angular resolution observations in regions where EME
is present. Another example of excellent correspondence between AME
and PAH emission is shown clearly in LDN1246 (Scaife et al. 2010)."



> - 3rd-to-last paragraph: are you confident that the correlation
    analysis with WISE is not affected by the interferometric
    filtering?

It must be but we hope that any bias is small. The main source of PSF
sidelobes is due to flux loss from missing antenna spacings at the
centre of the uv-plane.  Since we propose a strategy to recover the
missing spatial frequencies with {\tt skymem}, our hope is that the
restored ATCA mosaics are free to a good extent from such artefacts.
We have added more information to this effect:

" (...) The relatively coarse angular resolution of the WISE images
(6.1arcsec at 3.4μm and 15arcsec at 12μm), compared to IRAC8μm
(2.5arcsec), prevents their filtering for the ATCA+skymem
response. But we can nonetheless degrade the WISE images to the
coarsest ATCA beam (at 17 GHz) for a multi-frequency comparison.  The
smoothed images are not exactly comparable to the ATCA mosaics, since
we have not filtered for the ATCA response. But we hope that any
resulting bias in the following analysis is small, since our synthesis
imaging strategy corrects for missing ATCA antenna spacings using a
prior image in skymem, and  the main source of PSF
sidelobes is due to flux loss from missing antenna spacings at the
Centre of the uv-plane (...)"


> - 2nd-to-last paragraph: is the offset of the 20 GHz point
    significant enough to be discussed?  It looks like ~1 sigma on the
    plot

Indeed, the difference is not significant when considering the 10%
calibration error. We have therefore removed this discussion.


> Fig 2, caption: Presumably the blue and green lines have also been
  scaled to match the radio intensity at each frequency

This is correct, the blue and green have been scaled by a reference radio-IR
correlation slope, chosen to be that in M\,78. We have reworded the
caption:

"Profiles from the restored ATCA mosaics, extracted perpendicularly to
the ρ Oph W PDR. Frequencies are indicated on the top left. The
following number is the radio-IR correlation slope a (same as in Table
2 and described in Sec. 2.2). The profile at each frequency is divided
by a. The x−axis shows offset in arcmin from the reference position,
at J2000 16:25:57.984 -24:20:37.760. y−axis shows specific intensities
averaged in a region ±1 arcmin along the filament. The ATCA profiles
are shown in red, with the restored image in solid line, its average
residuals in dotted line, and the rms-dispersion of residuals in
dashed line. The blue dashed line is the average profile of the IRAC
8μm template, which is the original IRAC image filtered and scaled by
a reference radio/IR correlation slop (see Sec. 2.2). The green dashed
line is the corresponding profile of a simulation of the ATCA
observations and skymem restoration."


> Section 4:
> - Paragraph 2: “signal cm-wavelength signal” - remove first “signal”

Thanks.

> - Second-to-last paragraph: Any prospects for observations with
    another instrument with better frequency resolution?  ALMA?

We added this material at the end of the Para:

"However, the expected CRRLs intensities should be within easy reach
with the Atacama Large Millimetre Array (ALMA), as long as the
spectral resolution is not degraded much beyond ∼0.5 km s −1. The
spectra line data could be acquired as part of future observations to
map the EME signal in ρ Oph W at ∼40 GHz with the Band 1 receivers
currently under construction, and which should yield a noise level of
3 mJy beam −1 in 40 mn and in 0.5 km s −1 channels."

> - Last paragraph: “$Te$” —> “$T_{\mathrm{e}}$”.

Thanks.

> What is the implication of the T_e<100K calculation for the observed
  signal?  “LT” —> “LTE”.  What is b?

We meant that the signal could be fainter if Te<100K. But we see now
that this is a bit of a distraction, since we do not really treat this
case, and since the nebular temperature is modeled to be ~300K by
Habart+ 2003. It might still be useful to provide the reader with an
idea of the range of validity of LTE in these estimates, and to
explain that the below 100K the signal could be fainter.  We therefore
kept the mention of the departure coefficients but moved it to a
footnote:

"The LTE deviations become important for n = 72 at Te < 100 K, i.e. the
n = 72 population departure coefficient relative to LTE is b < 1 and the
emergent intensities are proportionally fainter"


> Appendix A:
> - This is a really interesting method which could be useful in other
    cases.  Would you consider making the code public?

Sharing the code so that others can run it easily may be
difficult. The core of the code itself is actually quite simple, but
the bulk of it is application-specific book-keeping. In order to make
this code portable it should include scripts to generate the required
input data. This could be done with Miriad, but should probably be
ported to CASA. Another hurdle is that the code is written in Perl,
which is scarcely in use nowadays.

We have nonetheless added this note in data availability:

"The corresponding author will share the data underlying this article
on reasonable request, and will also provide help to researchers
interested in porting {\tt skymem} to other applications."


> - It would be great to see a dirty map, prior image, model image,
  residual image and clean image for one or two of the frequencies to
  see how well the method works

We thank the referee for this idea. The summary panel at the end of
the appendix does support our analysis of the ATCA data. 


> General remarks:

> - Please check consistency throughout in how you refer to the ATCA
    data.  It’s referred to variously as ATCA, ATCA+CABB, and CABB
    which may be confusing for someone not familiar with ATCA and its
    instrumentation.  I think it is all CABB data so just referring to
    it as ATCA would be fine.

Thanks, we now refer to these new data as the ATCA data, and use CABB
only when referring to the correlator setup. Our insistence on the
mention of CABB stemmed from these being among the first few
observations carried out with CABB, 10 years ago.


> - Tables: please put descriptions of table columns into caption
    rather than footnotes

We were instructed by MNRAS editors to format the table in this way,
using footnotes to keep the Table captions short. But this comment
from the referee suggests that the format is different, we will
therefore cross check with the editor. Once confirmed we will proceed
with adapting the tables.

----------------------------------------------------------------------
Additional changes:


- Added references for CABB and Miriad to Para 1 of Sec. 2.1.

- Added Sec. 2.2 on the point sources in the field. 

- Added new Table A2 with a comparison between the rms dispersion of
  residuals and those expected from the ATCA ETC.

- Added a Para at the end of Sec. 3.1 to conclude on the morphological
  trends and to discuss the latest results in Lbda Ori, where
  Cepeda-Arroita+ 2020 also report spectral variations in the EME
  spectrum. 
