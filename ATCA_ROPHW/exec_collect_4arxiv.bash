dir='upload_4arxiv/'
rm -rf $dir
mkdir $dir
mkdir $dir\figs
mkdir $dir\tables
for afile in    ./figs/roph_ATCA_2019.pdf     ./figs/ROPHW_profiles.pdf     ./figs/atca_rg_85_80.pdf     ./figs/SED.pdf     ./figs/GSD_shrunk.pdf     ./figs/12-3_80-85countours_b.pdf     
do
    rsync -va $afile  $dir\figs/
done
for afile in        ./tables/intensities_sed_ori.tex     ./tables/sed_params_ori.tex
do
    rsync -va $afile  $dir\tables/
done

rsync -va report_ATCA_ROPHW_4arxiv.tex  $dir
rsync -va report_ATCA_ROPHW.bbl  $dir

tar cvfz ball4xxx.tgz ./$dir
